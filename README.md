Livelevel
==========================

### Run in docker
1. `cp docker-compose.dist.yml docker-compose.yml`
2. Optional `cp protected/config/example_server.php protected/config/server.local.php`
3. Set permissions (Linux):
```
sudo find public/assets protected/runtime -type d -exec chmod 777 {} \;
```
4. `docker-compose up -d --build`
5. Wait for mysql and run migration
```
docker cp other/livelevel.sql $(docker-compose ps -q mysql):/tmp/dump.sql
docker-compose exec mysql sh -c "mysql livelevel -ulivelevel -plivelevel < /tmp/dump.sql"
```

### License MIT
    The MIT License

    Copyright (C) 2013 Roman Nix

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
