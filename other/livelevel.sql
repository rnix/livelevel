-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 08 2020 г., 06:30
-- Версия сервера: 5.7.16
-- Версия PHP: 5.6.27-1~dotdeb+zts+7.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `livelevel`
--

-- --------------------------------------------------------

--
-- Структура таблицы `achievement`
--

CREATE TABLE IF NOT EXISTS `achievement` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text,
  `desc` text,
  `points` int(10) unsigned DEFAULT NULL,
  `author_id` int(10) unsigned DEFAULT NULL,
  `ins_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `need_proof` tinyint(4) DEFAULT '0',
  `public` tinyint(4) DEFAULT NULL,
  `approved_by` int(10) unsigned DEFAULT NULL,
  `mass_koef` float DEFAULT NULL,
  `auto_ach_on_depend` tinyint(4) DEFAULT '0' COMMENT 'auto achieve when all dependencies are achieved',
  `app_client_id` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ach_author_id_fk1` (`author_id`),
  KEY `ach_approved_by_fk2` (`approved_by`),
  KEY `ach_app_fk3` (`app_client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `achievement`
--

INSERT INTO `achievement` (`id`, `name`, `desc`, `points`, `author_id`, `ins_date`, `need_proof`, `public`, `approved_by`, `mass_koef`, `auto_ach_on_depend`, `app_client_id`) VALUES
(1, 'Сделать себе портфолио', 'Нужно оформить, выложить в открытый доступ свои работы и дать на них ссылку.', 10, NULL, '2013-03-11 14:25:21', 0, 1, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `achievement_depend`
--

CREATE TABLE IF NOT EXISTS `achievement_depend` (
  `idach_depend` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idach` int(10) unsigned NOT NULL,
  `idach_depends_on_id` int(10) unsigned NOT NULL,
  `ins_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idach_depend`),
  KEY `ad_ach1_fk` (`idach`),
  KEY `ad_ach2_fk` (`idach_depends_on_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `achievement_tag`
--

CREATE TABLE IF NOT EXISTS `achievement_tag` (
  `idach` int(10) unsigned NOT NULL,
  `idtag` int(10) unsigned NOT NULL,
  `ins_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `at_ach_fk` (`idach`),
  KEY `at_tag_fk` (`idtag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `achievement_tag`
--

INSERT INTO `achievement_tag` (`idach`, `idtag`, `ins_date`) VALUES
(1, 1, '2013-03-11 14:25:21'),
(1, 2, '2013-03-11 14:25:21');

-- --------------------------------------------------------

--
-- Структура таблицы `oauth2_clients`
--

CREATE TABLE IF NOT EXISTS `oauth2_clients` (
  `client_id` varchar(20) NOT NULL,
  `client_secret` varchar(20) NOT NULL,
  `redirect_uri` varchar(200) NOT NULL,
  `app_owner_user_id` int(10) unsigned NOT NULL,
  `app_title` varchar(255) NOT NULL DEFAULT '',
  `app_desc` text,
  `status` int(1) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`client_id`),
  KEY `oa2_author_fk1` (`app_owner_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oauth2_clients`
--

-- --------------------------------------------------------

--
-- Структура таблицы `oauth2_tokens`
--

CREATE TABLE IF NOT EXISTS `oauth2_tokens` (
  `oauth_token` varchar(40) NOT NULL,
  `token_type` enum('code','access','refresh') DEFAULT 'code',
  `client_id` varchar(20) NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `expires` int(11) NOT NULL,
  `redirect_uri` varchar(200) NOT NULL DEFAULT 'oob',
  `scope` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`oauth_token`),
  KEY `user_id` (`user_id`),
  KEY `oa2_client_fk1` (`client_id`),
  KEY `oa2_user_fk2` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oauth2_tokens`
--
-- --------------------------------------------------------

--
-- Структура таблицы `tag`
--

CREATE TABLE IF NOT EXISTS `tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(90) NOT NULL,
  `ins_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `author_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Дамп данных таблицы `tag`
--

INSERT INTO `tag` (`id`, `name`, `ins_date`, `author_id`) VALUES
(1, 'портфолио', '2013-03-11 14:16:37', 3),
(2, 'свои работы', '2013-03-11 14:16:37', 3),
(3, 'тайланд', '2013-03-11 14:37:46', 2),
(4, 'путешествия', '2013-03-11 14:37:46', 2),
(5, 'thailand', '2013-03-11 14:37:46', 2),
(6, 'thai', '2013-03-11 14:37:46', 2),
(7, 'тай', '2013-03-11 14:37:46', 2),
(8, 'twitter', '2013-03-13 14:55:04', 1),
(9, 'Blizzard', '2013-03-14 02:34:57', 5),
(10, 'Games', '2013-03-14 02:34:57', 5),
(11, 'день рождения', '2013-03-14 03:09:56', 7),
(12, 'maze', '2013-03-26 06:19:23', 1),
(13, 'шашки', '2013-05-05 15:23:19', 1),
(14, 'интернет', '2014-01-22 09:57:37', 1),
(15, 'хабр', '2014-01-22 09:57:37', 1),
(16, 'игры', '2014-01-22 10:04:55', 1),
(17, 'друзья', '2014-01-22 10:04:55', 1),
(18, 'города', '2014-01-22 10:08:55', 1),
(19, 'mysql', '2014-07-14 09:09:52', 5),
(20, 'internet', '2014-07-14 09:09:52', 5),
(21, 'программирование', '2014-07-14 09:09:52', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `external_id_twi` int(10) unsigned DEFAULT NULL,
  `external_id_vk` int(10) unsigned DEFAULT NULL,
  `screen_name` varchar(100) DEFAULT NULL,
  `profile_image_url` varchar(200) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `lang` varchar(10) DEFAULT NULL,
  `location` varchar(150) DEFAULT NULL,
  `ins_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `role` varchar(45) DEFAULT NULL,
  `level` int(10) unsigned DEFAULT '0',
  `points` int(11) DEFAULT NULL,
  `banned` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Дамп данных таблицы `user`
--

--
-- Структура таблицы `user_achievement`
--

CREATE TABLE IF NOT EXISTS `user_achievement` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `iduser` int(10) unsigned NOT NULL,
  `idach` int(10) unsigned NOT NULL,
  `ins_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ach_date` timestamp NULL DEFAULT NULL,
  `ach_approved_by` int(10) unsigned DEFAULT NULL,
  `ach_approved_date` timestamp NULL DEFAULT NULL,
  `proof_link1` varchar(120) DEFAULT NULL,
  `proof_link2` varchar(120) DEFAULT NULL,
  `comment` text,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ua_user_fk_1` (`iduser`),
  KEY `ua_ach_fk_2` (`idach`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Дамп данных таблицы `user_achievement`
--

-- --------------------------------------------------------

--
-- Структура таблицы `user_app`
--

CREATE TABLE IF NOT EXISTS `user_app` (
  `iduser_app` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(10) unsigned NOT NULL,
  `client_id` varchar(20) NOT NULL,
  `grant_status` tinyint(4) DEFAULT NULL,
  `ins_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `grant_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`iduser_app`),
  KEY `uapp_user_fk1` (`iduser`),
  KEY `uapp_app_fk2` (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Дамп данных таблицы `user_app`
--
-- --------------------------------------------------------

--
-- Структура таблицы `user_points`
--

CREATE TABLE IF NOT EXISTS `user_points` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `iduser` int(10) unsigned NOT NULL,
  `idach` int(10) unsigned DEFAULT NULL,
  `action` varchar(45) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `ins_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `up_uid_fk1` (`iduser`),
  KEY `up_achid_fk2` (`idach`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Дамп данных таблицы `user_points`
--

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `achievement`
--
ALTER TABLE `achievement`
  ADD CONSTRAINT `ach_app_fk3` FOREIGN KEY (`app_client_id`) REFERENCES `oauth2_clients` (`client_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ach_approved_by_fk2` FOREIGN KEY (`approved_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ach_author_id_fk1` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `achievement_depend`
--
ALTER TABLE `achievement_depend`
  ADD CONSTRAINT `ad_ach1_fk` FOREIGN KEY (`idach`) REFERENCES `achievement` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ad_ach2_fk` FOREIGN KEY (`idach_depends_on_id`) REFERENCES `achievement` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `achievement_tag`
--
ALTER TABLE `achievement_tag`
  ADD CONSTRAINT `at_ach_fk` FOREIGN KEY (`idach`) REFERENCES `achievement` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `at_tag_fk` FOREIGN KEY (`idtag`) REFERENCES `tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oauth2_clients`
--
ALTER TABLE `oauth2_clients`
  ADD CONSTRAINT `oa2_author_fk1` FOREIGN KEY (`app_owner_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `oauth2_tokens`
--
ALTER TABLE `oauth2_tokens`
  ADD CONSTRAINT `oa2_client_fk1` FOREIGN KEY (`client_id`) REFERENCES `oauth2_clients` (`client_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `oa2_user_fk2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_achievement`
--
ALTER TABLE `user_achievement`
  ADD CONSTRAINT `ua_ach_fk_2` FOREIGN KEY (`idach`) REFERENCES `achievement` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ua_user_fk_1` FOREIGN KEY (`iduser`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_app`
--
ALTER TABLE `user_app`
  ADD CONSTRAINT `uapp_app_fk2` FOREIGN KEY (`client_id`) REFERENCES `oauth2_clients` (`client_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `uapp_user_fk1` FOREIGN KEY (`iduser`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_points`
--
ALTER TABLE `user_points`
  ADD CONSTRAINT `up_achid_fk2` FOREIGN KEY (`idach`) REFERENCES `achievement` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `up_uid_fk1` FOREIGN KEY (`iduser`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
