<?php

return array(
    'params'=>array(
        'adminEmail'=>'rnix@yandex.ru',
        'private' => [
            // Twitter credentials
            'CONSUMER_KEY' => '',
            'CONSUMER_SECRET' => '',
        ],
    ),
    'components' => array(
        'eauth' => array(
            'services' => array(
                'vkontakte' => array(
                    'client_id' => '123456',
                    'client_secret' => 'adDggjaRghnTY44F',
                ),
            ),
        ),
    ),
);
