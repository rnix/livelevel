<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
Yii::setPathOfAlias('widgets', dirname(__FILE__).'/../components/widgets');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'LiveLevel',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'ext.twitteroauth.*',
        'ext.eoauth.*',
        'ext.eoauth.lib.*',
        'ext.eauth.*',
        'ext.eauth.services.*',
        'ext.oauth2.*',
	),
	'theme'=>'bootstrap',

	'modules'=>array(
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'Y2k',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
			'generatorPaths'=>array(
                'bootstrap.gii',
            ),
		),
        'api'=>array(),
	),

	// application components
	'components'=>array(
		'user'=>array(
		    'class' => 'WebUser',
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
            'loginUrl'=>array('login/index'),
		),
		'urlManager'=>array(
			'urlFormat'=>'path',
                        'showScriptName' => false,
			'rules'=>array(
                                '' => 'achievement/index',
				'id<id:\d+>'=>'achievement/view',
				'user/<userId:\d+>'=>'users/byId',
				'user/<userId:\d+>/<userName:\w+>'=>'users/byId',
				'user/<userName:\w+>'=>'users/byName',
				'app/<appId:\d+>'=>'site/app',
				'rules'=>'site/page/view/rules',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				'api/<controller:\w+>/<action:\w+>/<id:\d+>'=>'api/<controller>/<action>',
			),
		),
        'db' => array(
            'connectionString' => 'mysql:host=mysql;dbname=livelevel',
            'emulatePrepare' => true,
            'username' => 'livelevel',
            'password' => 'livelevel',
            'charset' => 'utf8mb4',
        ),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
		'bootstrap'=>array(
                    'class'=>'bootstrap.components.Bootstrap',
                ),
                'request'=>array(
                    'class'=>'application.components.HttpRequest',
                    'enableCsrfValidation'=>true,
                    'noCsrfValidationRoutes'=>array('api/*'),
                ),
                'authManager'=>array(
                    'class' => 'PhpAuthManager',
                    'defaultRoles' => array('guest'),
                ),
                'eauth' => array(
                    'class' => 'ext.eauth.EAuth',
                    'popup' => true,
                    'cache' => false,
                    'cacheExpire' => 0,
                    'services' => array(
                        'vkontakte' => array(
                            'class' => 'MyVKontakteService',
                            'client_id' => '...',
                            'client_secret' => '...',
                        ),
                    ),
                ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
        'private' => [
            'CONSUMER_KEY' => '',
            'CONSUMER_SECRET' => '',
        ],
        'achievePoints' => array(
            'unproven' => 1,
            'base' => 10,
            'achieved_by_other' => 1,
            'achieved_and_proved_by_other' => 2,
        ),
        'safeDeleteLimit' => 5,
	),
    'language' => 'ru',
);
