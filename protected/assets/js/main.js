$(function(){
    
    var currentSelectedTags = [];
    
    if ($(".tags-list").length){
        
        $("a[href='#']").live('click', function(e) {
            e.preventDefault();
        });
    
        $(".tags-list .search-tag a").live('click', function(){
            var st = $(this).hasClass("active");
            var tag = $(this).attr('data-tag');
            
            if (st){
                $(this).removeClass('active');
                removeSearchTag(tag);
            } else {
                    $(this).addClass('active');
                    addSearchTag(tag, true);
                }
        });
        
        $(".selected-tags-list li").live('click', function(){
            removeSearchTag($(this).attr('data-tag'));
        });
    }
    
    var addSearchTag = function(tag, needUpdate){
        currentSelectedTags.push(tag);
        var $tpl = $("#selected-tag-tpl").clone();
        $tpl.removeAttr('id');
        $tpl.attr("data-tag", tag);
        var $container = $("#selected-tag-tpl").closest("ul");
        $tpl.prepend(tag).appendTo($container);
        $tpl.show();
        if (needUpdate){
            updateAchList();
        }
    };
    
    var removeSearchTag = function(tag){        
        $(".tags-list .search-tag a[data-tag='" + tag + "']").removeClass('active');
        $(".selected-tags-list li[data-tag='" + tag + "']").remove();
        updateAchList();
        
        var tmpTagsArray = [];
        for (var i = 0; i < currentSelectedTags.length; i++) {
            if (tag !== currentSelectedTags[i]) {
                tmpTagsArray.push(tag);
            }
        }
        currentSelectedTags = tmpTagsArray;
    };
    
    var updateAchList = function(){
        var tags = [];
        $(".selected-tags-list li").each(function(i, el){
            var tag = $(el).attr('data-tag');
            if (tag){
                tags.push(tag);
            }
        });
        
        $.get('/achievement/list', {'tags': tags}, function(data){
           $(".ach-list").html(data); 
        });
        setHashTags(tags);
    };
    
    if ($("#search-tags-input").length){
        $("#search-tags-input").keyup(function(e){
            var val = $(this).val();
            loadTagListByTerm(val);
        });
        
        var loadTagListByTerm = function(term){
            $.get('/json/SuggestTags', {
                'tag': term
            }, function(data){
                var tags = $.parseJSON(data);
                var $tplSource = $("#search-tag-tpl").clone().removeAttr('id');
                var $container = $("#search-tag-tpl").closest("ul");
                var $tpl;
                $container.find('li:visible').remove();
                
                if (tags.length){
                    $("#search-tags-not-found").hide();
                    $container.show();
                    $.each(tags, function(i, tag){
                        $tpl = $tplSource.clone();
                        $tpl.find('a').prepend(tag.value);
                        $tpl.find('a').attr('data-tag', tag.value);
                        $tpl.appendTo($container);
                        $tpl.show();
                        
                        if (currentSelectedTags.indexOf(tag.value) > -1){
                            $tpl.find('a').addClass('active');
                        }
                    });
                    
                } else {
                    $("#search-tags-not-found").show();
                    $container.hide();
                }
            });
        };
    }
    
    var setHashTags = function(tags){
        location.hash = '#tags=' + tags.join('/');
    };
    
    var getHashTags = function(){
        var hash = location.hash.slice(1);
        if (hash){
            var match = hash.match(/tags=(.+)/);
            if (match && typeof match[1] != 'undefined'){
                return match[1].split('/');
            }
        }
        return false;
    };
    
    var loadHashTags = function() {
        var hashTags = getHashTags();
        if (hashTags) {
            for (var i = 0; i<hashTags.length; i++) {
                addSearchTag(hashTags[i], false);
                $(".tags-list .search-tag a[data-tag='" + hashTags[i] + "']").addClass('active');
            }
            updateAchList();
        }
    };
    loadHashTags();
    
    if ($(".selected-tags-list").length){
        $(".ach-container .tags a").live('click', function(){
            addSearchTag($(this).attr('data-tag'), true);
        });
    }
    
    /* latest actions */
    if ($(".latest-actions-list").length){
        var latestId, $li;
        var limit = 15;
        var addLatestRows = function(data){
            var rows = $.parseJSON(data);
            var size = $(".latest-actions-list li").length;
            $.each(rows, function(i, row){
                latestId = row.id;
                if (size >= limit){
                    $(".latest-actions-list li:last").remove();
                }
                $li = $('<li>');
                $li.html(row.content);
                $li.hide();
                $(".latest-actions-list").prepend($li);
                $li.show('slow');
            });
        };
        
        var loadLatest = function(){
            $.get("/json/latestActions", {'latestId': latestId}, addLatestRows);
        };
        window.setInterval(loadLatest, 10000);
        loadLatest();
    }
});