<?php

$app_env_file_name = dirname(__FILE__) . '/APPLICATION_ENV.php';
if (file_exists($app_env_file_name)) {
    defined('APPLICATION_ENV') || define('APPLICATION_ENV', include $app_env_file_name);
} else if (getenv('APPLICATION_ENV')) {
    defined('APPLICATION_ENV') || define('APPLICATION_ENV', getenv('APPLICATION_ENV'));
}

$configDir = dirname(__FILE__) . '/../protected/config';

if (defined('APPLICATION_ENV')) {
    switch (APPLICATION_ENV) {
        case 'dev_home':
            defined('YII_DEBUG') or define('YII_DEBUG', true);
            defined('YII_ENABLE_ERROR_HANDLER') or define('YII_ENABLE_ERROR_HANDLER', false);
            break;
        case 'dev_work':
            defined('YII_DEBUG') or define('YII_DEBUG', true);
            defined('YII_ENABLE_ERROR_HANDLER') or define('YII_ENABLE_ERROR_HANDLER', false);
            break;
        default:
            defined('YII_DEBUG') or define('YII_DEBUG', false);
            break;
    }
}

defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

require_once('/yii/yii-1.1.13/framework/yii.php');
$yiic = '/yii/yii-1.1.13/framework/yiic.php';

$config = require_once( $configDir . '/main.php' );
$localConfigFile = $configDir . '/server.local.php';
if (file_exists($localConfigFile)) {
    $configServer = require_once( $localConfigFile );
    $config = CMap::mergeArray($config, $configServer);
}

require_once($yiic);
