<?php

class ApiModule extends CWebModule {

    public $oAuthUserId;
    public $oAuthClientId;
    
    
    public function init() {
        $this->setImport(array(
            'api.components.*',
        ));
    }

   public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            if ($controller->id != 'oauth') {
                $oauth = MyYiiOAuth2::instance();
                $result = $oauth->verify();
                if (false !== $result){
                    $tokenInfo = $oauth->getTokenInfo();
                    $this->oAuthUserId = $tokenInfo['user_id'];
                    $this->oAuthClientId = $tokenInfo['client_id'];
                }
            }
            return true;
        }
        else
            return false;
    }

}
