<?php
/**
 * @property WebUser $user 
 * @property Achievement $ach 
 */
class AchievementsController extends ApiController {
    
    protected $postActions = array('achieve');
    protected $user;
    protected $ach;
    
    public function beforeAction($action){
        if (parent::beforeAction($action)){
            
            if ($this->oAuthUserId){
                $this->user = User::model()->findByPk($this->oAuthUserId);
            }
            $actionParams = $action->getController()->getActionParams();
            if (!empty($actionParams['id'])){
                $this->ach = Achievement::model()->findByPk($actionParams['id']);
            }
            
            return true;
        } else {
            return false;
        }
    }
    
    public function actionView($id){
        $model = Achievement::model()->findByPk($id);
        if ($model){
            echo CJSON::encode($model->convertToApiStdClass());
        } else {
            echo new ApiError('not_found', 'This achievement does not exist');
        }
    }
    
    public function actionOwned_by_app() {
        $models = Achievement::model()->findAll('app_client_id=:app_client_id', array(':app_client_id' => $this->oAuthClientId));
        $list = array();
        foreach ($models as $model) {
            $list[] = $model->convertToApiStdClass();
        }
        echo CJSON::encode($list);
    }
    
    public function actionGetTokenInfo(){
        $info = new stdClass();
        $info->oAuthClientId = $this->oAuthClientId;
        $info->oAuthUserId = $this->oAuthUserId;
        echo CJSON::encode($info);
    }
    
    public function actionAchieve() {
        $user = $this->user;
        $ach = $this->ach;
        if ($ach && $user) {
            if ($ach->app_client_id !== $this->oAuthClientId) {
                echo new ApiError('wrong_achievement', 'This achievement is not owned by this application.');
                return;
            }
            $uaModel = new UserAchievement;
            $uaModel->ach_date = date('d.m.Y', time());
            if (!$uaModel->validate()) {
                echo new ApiError('wrong_params', 'Params not valid', $uaModel->getErrors());
                return;
            }
            if ($ach->isAchievedAndApproved($user)) {
                echo new ApiError('double_operation', 'User has already achieved');
                return;
            }
            $uaModel->idach = $ach->id;
            $uaModel->iduser = $user->id;
            $uaModel->save();
            $uaModel->approveByApp($this->oAuthClientId);
            echo CJSON::encode($uaModel->convertToApiStdClass());
        } else {
            echo new ApiError('not_found', 'Achievement does not exist');
        }
    }
    
    public function actionFail() {
        if ($this->ach && $this->user) {
            if ($this->ach->app_client_id !== $this->oAuthClientId){
                echo new ApiError('wrong_achievement', 'This achievement is not owned by this application.');
                return;
            }
            $this->ach->fail($this->user);
            echo CJSON::encode($this->ach->convertToApiStdClass());
        } else {
            echo new ApiError('not_found', 'Achievement does not exist');
        }
    }
}