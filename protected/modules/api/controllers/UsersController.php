<?php

class UsersController extends ApiController {
    
    public function actionView($id){
        $model = User::model()->findByPk($id);
        if ($model){
            echo CJSON::encode($model->convertToApiStdClass());
        } else {
            echo new ApiError('not_found', 'This user does not exist');
        }
    }
    
    
    public function actionInfo(){
        $this->actionView($this->oAuthUserId);
    }
}