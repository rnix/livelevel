<?php

class ApiController extends Controller {

    public $oAuthUserId;
    public $oAuthClientId;
    protected $postActions = array();
    
    protected function _mustBePost($action){
        return in_array($action->getId(), $this->postActions);
    }


    public function beforeAction($action){
        if (parent::beforeAction($action)){
            
            $this->oAuthClientId = Yii::app()->controller->module->oAuthClientId;
            $this->oAuthUserId = Yii::app()->controller->module->oAuthUserId;
            
            if ($this->_mustBePost($action) && $_SERVER['REQUEST_METHOD'] !== 'POST'){
                 echo new ApiError('not_post', 'This method must be POST');
                 return false;
            }
            
            return true;
        } else {
            return false;
        }
    }
    
}