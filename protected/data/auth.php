<?php
return array (
  'userManage' => 
  array (
    'type' => 1,
    'description' => 'управление пользователями',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'grantModeration',
      1 => 'revokeModeration',
      2 => 'banUser',
    ),
  ),
  'grantModeration' => 
  array (
    'type' => 0,
    'description' => 'сделать модератором',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'revokeModeration' => 
  array (
    'type' => 0,
    'description' => 'отобрать модераторорство',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'banUser' => 
  array (
    'type' => 0,
    'description' => 'заблокировать пользователя',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'approveAchieve' => 
  array (
    'type' => 1,
    'description' => 'подтверждение достижения',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'approve',
      1 => 'reject',
    ),
  ),
  'approve' => 
  array (
    'type' => 0,
    'description' => 'принять',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'reject' => 
  array (
    'type' => 0,
    'description' => 'отклонить',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'addAchievement' => 
  array (
    'type' => 0,
    'description' => 'добавить достижение',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'deleteAnyAchievement' => 
  array (
    'type' => 0,
    'description' => 'удалить любое достижение',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'deleteAchievement' => 
  array (
    'type' => 0,
    'description' => 'удалить достижение',
    'bizRule' => 'return $params["achievement"]->isAbleToSafeDeleted() && Yii::app()->user->checkAccess("deleteAnyAchievement") 
            || (Yii::app()->user->id==$params["achievement"]->author_id && $params["achievement"]->isAbleToSafeDeleted());',
    'data' => NULL,
  ),
  'guest' => 
  array (
    'type' => 2,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'user' => 
  array (
    'type' => 2,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'addAchievement',
      1 => 'deleteAchievement',
    ),
  ),
  'moderator' => 
  array (
    'type' => 2,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'user',
      1 => 'approveAchieve',
    ),
  ),
  'admin' => 
  array (
    'type' => 2,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'moderator',
      1 => 'userManage',
      2 => 'approveMyAchieve',
      3 => 'deleteAnyAchievement',
    ),
  ),
  'approveMyAchieve' => 
  array (
    'type' => 0,
    'description' => 'подтверждать свои достижения',
    'bizRule' => NULL,
    'data' => NULL,
  ),
);
