<?php

/**
 * This is the model class for table "achievement_tag".
 *
 * The followings are the available columns in table 'achievement_tag':
 * @property string $idach
 * @property string $idtag
 * @property string $ins_date
 *
 * The followings are the available model relations:
 * @property Achievement $idach0
 * @property Tag $idtag0
 */
class AchievementTag extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AchievementTag the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'achievement_tag';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idach, idtag', 'length', 'max'=>10),
			array('ins_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idach, idtag, ins_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idach0' => array(self::BELONGS_TO, 'Achievement', 'idach'),
			'idtag0' => array(self::BELONGS_TO, 'Tag', 'idtag'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idach' => 'Idach',
			'idtag' => 'Idtag',
			'ins_date' => 'Ins Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idach',$this->idach,true);
		$criteria->compare('idtag',$this->idtag,true);
		$criteria->compare('ins_date',$this->ins_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}