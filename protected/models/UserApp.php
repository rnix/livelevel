<?php

/**
 * This is the model class for table "user_app".
 *
 * The followings are the available columns in table 'user_app':
 * @property integer $iduser_app
 * @property string $iduser
 * @property string $client_id
 * @property integer $grant_status
 * @property string $ins_date
 * @property string $grant_date
 *
 * The followings are the available model relations:
 * @property User $user
 * @property AppOAuth $app
 */
class UserApp extends CActiveRecord {

    const GRANT_STATUS_GRANTED = 1;
    const GRANT_STATUS_NONE = 0;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UserApp the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user_app';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('grant_status', 'numerical', 'integerOnly' => true),
            array('iduser', 'length', 'max' => 10),
            array('client_id', 'length', 'max' => 20),
            array('ins_date, grant_date', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('iduser_app, iduser, client_id, grant_status, ins_date, grant_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'iduser'),
            'app' => array(self::BELONGS_TO, 'AppOAuth', 'client_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'iduser_app' => 'Iduser App',
            'iduser' => 'Iduser',
            'client_id' => 'Client',
            'grant_status' => 'Grant Status',
            'ins_date' => 'Ins Date',
            'grant_date' => 'Grant Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('iduser_app', $this->iduser_app);
        $criteria->compare('iduser', $this->iduser, true);
        $criteria->compare('client_id', $this->client_id, true);
        $criteria->compare('grant_status', $this->grant_status);
        $criteria->compare('ins_date', $this->ins_date, true);
        $criteria->compare('grant_date', $this->grant_date, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}