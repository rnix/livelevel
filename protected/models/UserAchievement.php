<?php

/**
 * This is the model class for table "user_achievement".
 *
 * The followings are the available columns in table 'user_achievement':
 * @property string $iduser
 * @property string $idach
 * @property string $ins_date
 * @property string $ach_date
 * @property string $ach_approved_by
 * @property string $ach_approved_date
 * @property string $proof_link
 * @property string $status
 * @property string $comment
 *
 * The followings are the available model relations:
 * @property Achievement $ach
 * @property User $user
 */
class UserAchievement extends CActiveRecord {
    
    const STATUS_APPROVED = 1;
    const STATUS_REJECTED = 2;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UserAchievement the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user_achievement';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('proof_link1, proof_link2', 'length', 'max' => 120),
            array('proof_link1, proof_link2', 'url'),
            array('ach_date', 'required'),
            array('ach_date', 'type', 'type' => 'date', 'dateFormat' => 'yyyy-MM-dd'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('iduser, idach, ins_date, ach_date, points, ach_approved_by, ach_approved_date, proof_link', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'ach' => array(self::BELONGS_TO, 'Achievement', 'idach'),
            'user' => array(self::BELONGS_TO, 'User', 'iduser'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'iduser' => 'Iduser',
            'idach' => 'Idach',
            'ins_date' => 'Ins Date',
            'ach_date' => 'Дата достижения',
            'ach_approved_by' => 'Ach Approved By',
            'ach_approved_date' => 'Ach Approved Date',
            'proof_link1' => 'Первая ссылка на подтверждающую страницу',
            'proof_link2' => 'Вторая ссылка на подтверждающую страницу',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('iduser', $this->iduser, true);
        $criteria->compare('idach', $this->idach, true);
        $criteria->compare('ins_date', $this->ins_date, true);
        $criteria->compare('ach_date', $this->ach_date, true);
        $criteria->compare('points', $this->points, true);
        $criteria->compare('ach_approved_by', $this->ach_approved_by, true);
        $criteria->compare('ach_approved_date', $this->ach_approved_date, true);
        $criteria->compare('proof_link', $this->proof_link, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }
    
    protected function afterFind() {
        // convert to display format
        $this->ach_date = strtotime($this->ach_date);
        $this->ach_date = date('d.m.Y', $this->ach_date);

        parent::afterFind();
    }
    
    protected function beforeValidate() {
        // convert to storage format
        $this->ach_date = strtotime($this->ach_date);
        $this->ach_date = date('Y-m-d', $this->ach_date);

        return parent::beforeValidate();
    }

    protected function _approveAction($moderUser) {
        $this->ach_approved_date = new CDbExpression('NOW()');
        $this->ach_approved_by = $moderUser->getId();
    }
    
    public function approve($moderUser) {
        $this->_approveAction($moderUser);
        $this->status = self::STATUS_APPROVED;
        $this->save();
        $this->user->addPoints($this->ach->getProvenAchievePoints(), UserPoints::ACTION_ACHIEVED_AND_PROVED, $this->ach->id);
        if ($this->user->id != $this->ach->author->id) {
            $this->ach->author->addPoints(Yii::app()->params['achievePoints']['achieved_and_proved_by_other'], UserPoints::ACTION_ACHIEVED_AND_PROVED_BY_OTHER, $this->ach->id);
        }
    }
    
    public function approveByApp($appId) {
        $this->status = self::STATUS_APPROVED;
        $this->ach_approved_date = new CDbExpression('NOW()');
        $this->save();
        $this->user->addPoints($this->ach->getProvenAchievePoints(), UserPoints::ACTION_ACHIEVED_AND_PROVED, $this->ach->id);
    }
    
    public function reject($moderUser, $comment = ''){
        $this->_approveAction($moderUser);
        $this->status = self::STATUS_REJECTED;
        $this->comment = $comment;
        $this->save();
    }
    
    public function countPoints() {
        $points = Yii::app()->db->createCommand()
                ->select('SUM(points)')
                ->from('user_points')
                ->where('iduser=:iduser AND idach=:idach', array(':iduser' => $this->iduser, ':idach'=>$this->idach))
                ->queryScalar();
        return $points;
    }
    
    public static function countForApprove(){
        $criteria = new CDbCriteria();
        $criteria->condition = 'ach_approved_date IS NULL AND iduser <> :iduser AND status IS NULL';
        if (Yii::app()->user->checkAccess('approveMyAchieve')){
            $criteria->params = array(':iduser'=>0);
        } else {
            $criteria->params = array(':iduser'=>Yii::app()->user->getId());
        }
        return UserAchievement::model()->count($criteria);
    }
    
    public function behaviors() {
        return array(
            'ApiStdModelBehavior' => array(
                'class' => 'application.components.ApiStdModelBehavior',
                'publicApiProperties' => array(
                    'iduser',
                    'ach_date',
                    'ins_date',
                ),
            ),
        );
    }
}