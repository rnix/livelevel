<?php

/**
 * This is the model class for table "achievement".
 *
 * The followings are the available columns in table 'achievement':
 * @property string $id
 * @property string $name
 * @property string $desc
 * @property string $points
 * @property string $author_id
 * @property string $ins_date
 * @property integer $need_proof
 * @property integer $public
 * @property string $approved_by
 * @property double $mass_koef
 * @property integer $auto_ach_on_depend
 * @property integer $app_client_id
 *
 * The followings are the available model relations:
 * @property User $author
 * @property User $approvedBy
 * @property AchievementDepend[] $achievementDepends
 * @property AchievementDepend[] $achievementDepends1
 * @property Tag[] $tags
 * @property User[] $users
 * @property UserPoints[] $userPoints
 * @property AppOAuth $app
 */
class Achievement extends CActiveRecord {
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Achievement the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'achievement';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, desc, tags', 'required'),
            array('app_client_id', 'validateAppId'),
            array('public', 'boolean', 'on'=>'insert'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'achievementDepends' => array(self::HAS_MANY, 'AchievementDepend', 'idach'),
            'achievementDependsOn' => array(self::HAS_MANY, 'AchievementDepend', 'idach_depends_on_id'),
            'tags' => array(self::MANY_MANY, 'Tag', 'achievement_tag(idach, idtag)'),
            'users' => array(self::HAS_MANY, 'UserAchievement', 'idach'),
            'author' => array(self::BELONGS_TO, 'User', 'author_id'),
            'userPoints' => array(self::HAS_MANY, 'UserPoints', 'idach'),
            'approvedBy' => array(self::BELONGS_TO, 'User', 'approved_by'),
            'app' => array(self::BELONGS_TO, 'AppOAuth', 'app_client_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Наименование',
            'desc' => 'Описание',
            'points' => 'Points',
            'author_id' => 'Author',
            'ins_date' => 'Ins Date',
            'need_proof' => 'Need Proof',
            'public' => 'Public',
            'approved_by' => 'Approved By',
            'mass_koef' => 'Mass Koef',
            'auto_ach_on_depend' => 'Auto Ach On Depend',
            'app_client_id' => 'Достижение будет выполнятся через приложение',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('desc', $this->desc, true);
        $criteria->compare('points', $this->points, true);
        $criteria->compare('author_id', $this->author_id, true);
        $criteria->compare('ins_date', $this->ins_date, true);
        $criteria->compare('need_proof', $this->need_proof);
        $criteria->compare('public', $this->public);
        $criteria->compare('approved_by', $this->approved_by, true);
        $criteria->compare('mass_koef', $this->mass_koef);
        $criteria->compare('auto_ach_on_depend', $this->auto_ach_on_depend);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }
    
    public function scopes() {
        return array(
            'public' => array(
                'condition' => 'public=1',
            ),
        );
    }
    
    public function recently($limit = 5) {
        $this->getDbCriteria()->mergeWith(array(
            'order' => 'ins_date DESC',
            'limit' => $limit,
        ));
        return $this;
    }
    
    public function withTags($tags, $mustHaveAllTags = true) {
        if ($mustHaveAllTags) {
            
            $i = 0;
            $join = '';
            $conditions = array();
            $params = array();
            foreach ($tags as $tag) {
                $i++;
                $join .= "INNER JOIN achievement_tag AS at_$i ON at_$i.idach = t.id INNER JOIN tag AS tag_$i ON at_$i.idtag = tag_$i.id ";
                $conditions[] = "tag_$i.name LIKE :tag_name_$i";
                $params[":tag_name_$i"] = $tag;
            }
            
            $this->getDbCriteria()->mergeWith(array(
                    'join' => $join,
                    'condition' => implode(" AND ", $conditions),
                    'params' => $params,
                ));
            
        } else {
            $this->getDbCriteria()->mergeWith(array(
                'join' => 'JOIN achievement_tag ON achievement_tag.idach = t.id JOIN tag ON achievement_tag.idtag = tag.id',
            ));
            $this->getDbCriteria()->addInCondition('tag.name', $tags);
        }
        
        return $this;
    }
    
    public function achievedByUser(User $user) {
        $this->getDbCriteria()->mergeWith(array(
            'join' => 'JOIN user_achievement ON user_achievement.idach = t.id',
            'condition' => 'user_achievement.iduser = :userId',
            'params' => array(':userId' => $user->id),
        ));

        return $this;
    }
    
    public function addedByUser(User $user) {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 'author_id = :userId AND app_client_id IS NULL',
            'params' => array(':userId' => $user->id),
        ));

        return $this;
    }

    public function uniqueNameAndDesc($attribute, $params = array()) {
        if (!$this->hasErrors()) {
            $params['criteria'] = array(
                'condition' => '`desc` LIKE :desc',
                'params' => array(':desc' => $this->desc),
            );
            $validator = CValidator::createValidator('unique', $this, $attribute, $params);
            $validator->validate($this, array($attribute));
        }
    }
    
    
    public function behaviors() {
        return array(
            'ESaveRelatedBehavior' => array(
                'class' => 'ext.ESaveRelatedBehavior'),
            'ApiStdModelBehavior' => array(
                'class' => 'application.components.ApiStdModelBehavior',
                'publicApiProperties' => array(
                    'id',
                    'name',
                    'desc',
                    'points',
                    'app_client_id',
                    'tags',
                    'users',
                    'ins_date',
                )
            ),
        );
    }
    
    protected function afterSave(){
        parent::afterSave();
 
        AchievementDepend::model()->deleteAll('idach = :idach', array(':idach'=>$this->id));
        foreach($this->achievementDependsOn as $ach) {
            $achDep = new AchievementDepend;
            $achDep->idach = $this->id;
            $achDep->idach_depends_on_id = $ach->id;
            $achDep->save();
        }
    }
    
    public function getUserAchievement($user){
        if ($user){
            return UserAchievement::model()->find('iduser=:iduser AND idach=:idach AND status=:status', array(':iduser' => $user->id, ':idach'=>$this->id, ':status'=>  UserAchievement::STATUS_APPROVED));
        }
    }
    
    public function isAchievedAndApproved($user){
        $ua = $this->getUserAchievement($user);
        if ($ua && $ua->ach_approved_date && $ua->status == UserAchievement::STATUS_APPROVED){
            return true;
        } else {
            return false;
        }
    }
    
    public function getProvenAchievePoints(){
        return $this->points;
    }
    
    public function userIsAbleToAchive(User $user){
        if ($this->app_client_id){
            return false;
        }
        if ($this->isAchievedAndApproved($user)){
            return false;
        }
        if (count($this->achievementDepends)) {
            foreach ($this->achievementDepends as $achD) {
                if (!$achD->achDepends->isAchievedAndApproved($user)) {
                    return false;
                }
            }
        }
        return true;
    }
    
    public function validateAppId($attribute, $params) {
        if (!$this->hasErrors()) {
            if ($this->app_client_id) {
                $app = AppOAuth::model()->find('client_id=:client_id AND app_owner_user_id=:app_owner_user_id', array(
                    ':client_id' => $this->app_client_id,
                    ':app_owner_user_id' => Yii::app()->user->id,
                ));
                if (!$app->client_id) {
                    $this->addError('app_client_id', 'Указанное приложение не найдено.');
                }
            } else {
                $this->app_client_id = null;
            }
        }
    }
    
    /**
     * See ApiStdModelBehavior
     */
    public function getApiRelatedUsers(){
        return $this->users(array('condition'=>'status=' . UserAchievement::STATUS_APPROVED));
    }
    
    /**
     * We cannot allow delete ach if 
     * it has been approved more then $limit times.
     * 
     * @return boolean allow author delete this
     */
    public function isAbleToSafeDeleted() {
        $limit = Yii::app()->params['safeDeleteLimit'];
        $approvedRows = UserAchievement::model()->findAll(array(
            'condition' => 'idach=:idach AND status=:status',
            'params' => array(':idach' => $this->id, ':status' => UserAchievement::STATUS_APPROVED),
            'limit' => $limit,
                ));
        if (count($approvedRows) == $limit) {
            return false;
        }
        return true;
    }
    
    public function fail($user){
        $rows = UserAchievement::model()->findAll('iduser=:iduser AND idach=:idach AND status=:status', array(':iduser' => $user->id, ':idach'=>$this->id, ':status'=>  UserAchievement::STATUS_APPROVED));
        foreach ($rows as $row){
            $row->delete();
        }
        
        $rowsPoints = UserPoints::model()->findAll('iduser=:iduser AND idach=:idach AND action=:action', array(':iduser' => $user->id, ':idach'=>$this->id, ':action'=> UserPoints::ACTION_ACHIEVED_AND_PROVED));
        foreach ($rowsPoints as $row){
            $row->delete();
        }
    }
}