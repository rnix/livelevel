<?php

/*
 * The followings are the available model relations:
 * @property Achievement[] $achievementsAuthor
 * @property Achievement[] $achievementsApprover
 * @property UserAchievement[] $userAchievements
 * @property UserPoints[] $userPoints
 * @property UserApp[] $userApps
 * @property AppOAuth[] $ownedApps
 */

class User extends CActiveRecord {
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'user';
    }
    
    public function rules() {
        return array(
            array('external_id, screen_name, profile_image_url, name, lang, location', 'safe'),
        );
    }
    
    
    public function getProfileUrl(){
        return Yii::app()->createUrl('/user/' . CHtml::encode($this->id) . '/' . CHtml::encode($this->screen_name));
    }
    
    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'achievementsAuthor' => array(self::HAS_MANY, 'Achievement', 'author_id'),
            'achievementsApprover' => array(self::HAS_MANY, 'Achievement', 'approved_by'),
            'userAchievements' => array(self::HAS_MANY, 'UserAchievement', 'iduser'),
            'userPoints' => array(self::HAS_MANY, 'UserPoints', 'iduser'),
            'ownedApps' => array(self::HAS_MANY, 'AppOAuth', 'app_owner_user_id'),
            'userApps' => array(self::HAS_MANY, 'UserApp', 'iduser'),
        );
    }
    
    public function addPoints($points, $action, $idach = null){
        $up = new UserPoints;
        $up->iduser = $this->id;
        $up->points = intval($points);
        $up->action = $action;
        if ($idach){
            $up->idach = $idach;
        }
        $up->save();
    }
    
    public function countPoints() {
        $points = Yii::app()->db->createCommand()
                ->select('SUM(points)')
                ->from('user_points')
                ->where('iduser=:iduser', array(':iduser' => $this->id))
                ->queryScalar();
        return (int)$points;
    }
    
    public function updateCurrentLevel() {
        $points = $this->countPoints();
        for ($i = 0; $i < 50; $i++) {
            if (self::getAmountPointsForLevel($i) > $points) {
                break;
            }
        }
        $lvl = max($i - 1, 0);
        $this->points = $points;
        $this->level = $lvl;
        $this->save();
    }
    
    public static function getAmountPointsForLevel($lvl){
        if ($lvl == 0){
            return 0;
        }
        $amount = 30;
        for ($i=1; $i < $lvl; $i++){
            $amount += round( pow(2*$i + 8, 1.5) );
        }
        return (int) $amount;
    }
    
    public static function getTopList(){
        $criteria = new CDbCriteria();
        $criteria->limit = 50;
        $criteria->condition = "level > 0 AND ((role IS NULL) OR (role!='admin' AND role!='moderator'))";
        $criteria->order = 'level DESC, points DESC, ins_date';
        return User::model()->findAll($criteria);
    }
    
    public function countAchievedAndProoved(){
        $criteria = new CDbCriteria();
        $criteria->condition = 'ach_approved_date IS NOT NULL AND iduser = :iduser AND status = ' . UserAchievement::STATUS_APPROVED;
        $criteria->params = array(':iduser'=>$this->id);
        return UserAchievement::model()->count($criteria);
    }
    
    public function ban(){
        $this->banned = 1;
        $this->save();
    }
    
    public function getHash(){
        return md5($this->screen_name . ':' . $this->id . 'confirmSalt_');
    }
    
    public function getExternalProfileUrl(){
        if ($this->external_id_twi){
            return 'https://twitter.com/' . $this->screen_name;
        } else if ($this->external_id_vk){
            return 'https://vk.com/' . $this->screen_name;
        }
    }
    
    public function grantApp($idClient){
        if ($this->checkGrantApp($idClient)){
            return;
        }
        $uapp = new UserApp;
        $uapp->client_id = $idClient;
        $uapp->iduser = $this->id;
        $uapp->grant_status = UserApp::GRANT_STATUS_GRANTED;
        $uapp->grant_date = new CDbExpression('NOW()');
        $uapp->save();
    }
    
    public function checkGrantApp($idClient){
        $res = UserApp::model()->exists('iduser=:iduser AND client_id=:client_id AND grant_status=:grant_status', 
                array(
                    ':iduser'=>$this->id,
                    ':client_id'=>$idClient,
                    ':grant_status'=>UserApp::GRANT_STATUS_GRANTED,
                    ));
        return $res;
    }
    
    public function revokeApp($idClient){
        $uapp = UserApp::model()->find('iduser=:iduser AND client_id=:client_id AND grant_status=:grant_status', 
                array(
                    ':iduser'=>$this->id,
                    ':client_id'=>$idClient,
                    ':grant_status'=>UserApp::GRANT_STATUS_GRANTED,
                    ));
            if ($uapp) {
            $uapp->grant_status = UserApp::GRANT_STATUS_NONE;
            $uapp->save();

            //delete all user's tokens
            $command = Yii::app()->db->createCommand();
            $command->delete('oauth2_tokens', 'user_id=:user_id AND client_id=:client_id', array(':user_id'=>$this->id, ':client_id'=>$idClient));
        }
    }
    
    public function behaviors() {
        return array(
            'ApiStdModelBehavior' => array(
                'class' => 'application.components.ApiStdModelBehavior',
                'publicApiProperties' => array(
                    'id',
                    'screen_name',
                    'profile_image_url',
                    'full_name',
                    'location',
                    'points' => 'countPoints',
                    'level',
                ),
            ),
        );
    }
}