<?php

class AppOAuth extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'oauth2_clients';
    }

    public function rules() {
        return array(
            array('redirect_uri, app_title', 'required'),
            array('app_title', 'length', 'max' => 255),
            array('redirect_uri', 'length', 'max' => 200),
            array('client_secret', 'length', 'max' => 20),
            array('client_secret', 'length', 'min' => 10),
            array('redirect_uri', 'url'),
            array('app_desc', 'safe'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'app_title' => 'Название',
            'app_desc' => 'Описание',
            'redirect_uri' => 'URI возврата (адрес вашего сайта)',
            'client_secret' => 'Защищенный ключ',
        );
    }

    protected function _generateCredentials() {
        $uniqId = $this->model()->count();
        $this->client_id = ltrim(substr(substr(preg_replace('/[^0-9]/', '', md5(base64_encode(pack('N6', mt_rand(), mt_rand(), mt_rand(), mt_rand(), mt_rand(), uniqid())))), 0, 6) . $uniqId, -20), '0');
        $this->client_secret = substr(preg_replace('/[^0-9A-Za-z]/', '', base64_encode(pack('N6', mt_rand(), mt_rand(), mt_rand(), mt_rand(), mt_rand(), uniqid()))), 0, 20);
    }

    public function beforeSave() {
        if (parent::beforeSave()) {
            if (!$this->client_id) {
                $this->_generateCredentials();
            }
            return true;
        } else {
            return false;
        }
    }
    
    public function scopes() {
        return array(
            'active' => array(
                'condition' => 'status=1',
            ),
        );
    }

    public function behaviors() {
        return array(
            'ApiStdModelBehavior' => array(
                'class' => 'application.components.ApiStdModelBehavior',
            ),
        );
    }
}
