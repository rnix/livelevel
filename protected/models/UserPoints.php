<?php

/**
 * This is the model class for table "user_points".
 *
 * The followings are the available columns in table 'user_points':
 * @property string $id
 * @property string $iduser
 * @property string $idach
 * @property string $action
 * @property string $points
 * @property string $ins_date
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Achievement $ach
 */
class UserPoints extends CActiveRecord {
    
    const ACTION_ACHIEVED = 'achieved';
    const ACTION_ACHIEVED_AND_PROVED = 'achieved_and_proved';
    const ACTION_ACHIEVED_BY_OTHER = 'achieved_by_other';
    const ACTION_ACHIEVED_AND_PROVED_BY_OTHER = 'achieved_and_proved_by_other';
    

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UserPoints the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user_points';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('iduser', 'required'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, iduser, idach, action, points, ins_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'iduser'),
            'ach' => array(self::BELONGS_TO, 'Achievement', 'idach'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'iduser' => 'Iduser',
            'idach' => 'Idach',
            'action' => 'Action',
            'points' => 'Points',
            'ins_date' => 'Ins Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('iduser', $this->iduser, true);
        $criteria->compare('idach', $this->idach, true);
        $criteria->compare('action', $this->action, true);
        $criteria->compare('points', $this->points, true);
        $criteria->compare('ins_date', $this->ins_date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function afterSave(){
        parent::afterSave();
        $this->user->updateCurrentLevel();
    }
}