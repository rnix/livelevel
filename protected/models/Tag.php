<?php

/**
 * This is the model class for table "tag".
 *
 * The followings are the available columns in table 'tag':
 * @property string $id
 * @property string $name
 * @property string $ins_date
 * @property string $author_id
 *
 * The followings are the available model relations:
 * @property Achievement[] $achievements
 */
class Tag extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Tag the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tag';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'length', 'max' => 90),
            array('author_id', 'length', 'max' => 10),
            array('ins_date', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, ins_date, author_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'achievements' => array(self::MANY_MANY, 'Achievement', 'achievement_tag(idtag, idach)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'ins_date' => 'Ins Date',
            'author_id' => 'Author',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('ins_date', $this->ins_date, true);
        $criteria->compare('author_id', $this->author_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getPopular($limit = 5) {
        $connection=Yii::app()->db;
        $sql = "SELECT idtag, COUNT(idach) FROM achievement_tag GROUP BY idtag ORDER BY COUNT(idach) DESC LIMIT " . intval($limit);
        $dataReader = $connection->createCommand($sql)->query();
        $dataReader->bindColumn(1, $tagId);
        $modelsList = array();
        while ($dataReader->read() !== false) {
            $modelsList[] = $this->model()->findByPk($tagId);
        }
        return $modelsList;
    }
    
    public static function cleanName($name){
        $cleaned = trim($name);
        $cleaned = str_replace('/', '-', $cleaned);
        $cleaned = str_replace('\\', '-', $cleaned);
        $cleaned = preg_replace('/[^A-Za-z0-9А-Яа-яёЁ -]/u', '', $cleaned);
        $cleaned = preg_replace("/\s+/", " ", $cleaned);
        $cleaned = preg_replace("/-+/", "-", $cleaned);
        return $cleaned;
    }
    
    public function behaviors() {
        return array(
            'ApiStdModelBehavior' => array(
                'class' => 'application.components.ApiStdModelBehavior',
                'publicApiProperties' => array(
                    'name',
                ),
            ),
        );
    }
}