<?php

/**
 * This is the model class for table "achievement_depend".
 *
 * The followings are the available columns in table 'achievement_depend':
 * @property string $idach_depend
 * @property string $idach
 * @property string $idach_depends_on_id
 * @property string $ins_date
 *
 * The followings are the available model relations:
 * @property Achievement $idach0
 * @property Achievement $idachDependsOn
 */
class AchievementDepend extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AchievementDepend the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'achievement_depend';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idach, idach_depends_on_id', 'length', 'max'=>10),
			array('ins_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idach_depend, idach, idach_depends_on_id, ins_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idach0' => array(self::BELONGS_TO, 'Achievement', 'idach'),
			'achDepends' => array(self::BELONGS_TO, 'Achievement', 'idach_depends_on_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idach_depend' => 'Idach Depend',
			'idach' => 'Idach',
			'idach_depends_on_id' => 'Idach Depends On',
			'ins_date' => 'Ins Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idach_depend',$this->idach_depend,true);
		$criteria->compare('idach',$this->idach,true);
		$criteria->compare('idach_depends_on_id',$this->idach_depends_on_id,true);
		$criteria->compare('ins_date',$this->ins_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}