<?php

class LoginController extends Controller {
    
    protected function _saveTwiConnect() {
        /* If access tokens are not available redirect to connect page. */
        if (empty(Yii::app()->session['access_token']) || empty(Yii::app()->session['access_token']['oauth_token']) || empty(Yii::app()->session['access_token']['oauth_token_secret'])) {
            $this->redirect(array('login/clearsessions'));
        } else {
            $identity = new UserIdentityTwi();
            $identity->authenticate();
            
            if ($identity->errorCode === UserIdentity::ERROR_NONE){
                $duration = 3600 * 24 * 30; // 30 days
                Yii::app()->user->login($identity, $duration);
                $this->redirect(Yii::app()->user->returnUrl);
            }
        }
    }
    
    public function actionIndex(){
        return $this->actionLogin();
    }
    
    public function actionCallback() {
        /* If the oauth_token is old redirect to the connect page. */
        if (isset($_REQUEST['oauth_token']) && Yii::app()->session['oauth_token'] !== $_REQUEST['oauth_token']) {
            Yii::app()->session['oauth_status'] = 'oldtoken';
            $this->redirect(array('login/clearsessions'));
        }

        /* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
        $connection = new twitteroauth(
                Yii::app()->params['private']['CONSUMER_KEY'], 
                Yii::app()->params['private']['CONSUMER_SECRET'], 
                Yii::app()->session['oauth_token'], 
                Yii::app()->session['oauth_token_secret']);

        /* Request access tokens from twitter */
        if (empty($_REQUEST['oauth_verifier'])){
            $this->redirect(array('login/clearsessions'));
            return;
        }
        $access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);

        /* Save the access tokens. Normally these would be saved in a database for future use. */
        Yii::app()->session['access_token'] = $access_token;

        /* Remove no longer needed request tokens */
        unset(Yii::app()->session['oauth_token']);
        unset(Yii::app()->session['oauth_token_secret']);

        /* If HTTP response is 200 continue otherwise send to connect page to retry */
        if (200 == $connection->http_code) {
            /* The user has been verified and the access tokens can be saved for future use */
            Yii::app()->session['status'] = 'verified';
            $this->_saveTwiConnect();
        } else {
            /* Save HTTP status for error dialog on connnect page. */
            $this->redirect(array('login/clearsessions'));
        }
    }
    
    
    public function actionClearsessions() {
        unset(Yii::app()->session['oauth_token'], Yii::app()->session['oauth_token_secret'], Yii::app()->session['status']);
        $this->redirect(Yii::app()->homeUrl);
    }
    
    public function actionTwi() {
        /* Build TwitterOAuth object with client credentials. */
        $connection = new twitteroauth(Yii::app()->params['private']['CONSUMER_KEY'], Yii::app()->params['private']['CONSUMER_SECRET']);

        /* Get temporary credentials. */
        $request_token = $connection->getRequestToken($this->createAbsoluteUrl('login/callback'));

        /* Save temporary credentials to session. */
        Yii::app()->session['oauth_token'] = $token = $request_token['oauth_token'];
        Yii::app()->session['oauth_token_secret'] = $request_token['oauth_token_secret'];

        /* If last connection failed don't display authorization link. */
        switch ($connection->http_code) {
            case 200:
                /* Build authorize URL and redirect user to Twitter. */
                $url = $connection->getAuthorizeURL($token);
                header('Location: ' . $url);
                break;
            default:
                Yii::log('Invalid response from Twitter: ' . $connection->http_code . '; ' . json_encode($connection), CLogger::LEVEL_ERROR, 'AUTH');
                /* Show notification if something went wrong. */
                echo 'Could not connect to Twitter. Refresh the page or try again later.';
        }
    }
    
    public function actionLogout() {
        unset(Yii::app()->session['oauth_token'], Yii::app()->session['oauth_token_secret']);
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
    
    public function actionLogin(){
        $oauth2LoginReturnUrl = Yii::app()->user->getState('oauth2LoginReturnUrl');
        if ($oauth2LoginReturnUrl){
            Yii::app()->user->returnUrl = $oauth2LoginReturnUrl;
            Yii::app()->user->setState('oauth2LoginReturnUrl', null);
        }
        
        $this->render('selectProvider', array(
            'isGuest' => Yii::app()->user->isGuest,
            'canConnectTwi' => (!Yii::app()->user->isGuest && !Yii::app()->user->getModel()->external_id_twi),
            'canConnectVk' => (!Yii::app()->user->isGuest && !Yii::app()->user->getModel()->external_id_vk),
        ));
    }
    
    public function actionVK() {
        $authIdentity = Yii::app()->eauth->getIdentity('vkontakte');
        $authIdentity->redirectUrl = Yii::app()->user->returnUrl;
        $authIdentity->cancelUrl = $this->createAbsoluteUrl('login/selectProvider');

        if ($authIdentity->authenticate()) {
            $identity = new UserIdentityVK($authIdentity);
            
            if ($identity->authenticate()) {
                $duration = 3600 * 24 * 30; // 30 days
                Yii::app()->user->login($identity, $duration);

                // special redirect with closing popup window
                $authIdentity->redirect();
            } else {
                // close popup window and redirect to cancelUrl
                $authIdentity->cancel();
            }
        }

        // Something went wrong, redirect to login page
        $this->redirect(array('login/selectProvider'));
    }
}
