<?php

class SiteController extends Controller {

    public function actions() {
        return array(
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }
    
    public function actionApp($appId){
        $app = AppOAuth::model()->findByPk($appId);
        if (!$app){
            throw new CHttpException(404, 'Not found');
        }
        $ahcs = Achievement::model()->findAll('app_client_id=:app_client_id AND public=1', array(':app_client_id' => $app->client_id));
        $this->render('app', array(
            'app'=>$app, 
            'achs'=>$ahcs, 
            'user' => Yii::app()->user->getModel(),
            ));
    }

}