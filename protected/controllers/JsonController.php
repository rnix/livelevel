<?php

class JsonController extends Controller {

    
    
    public function actionIndex() {
    }

    public function actionSuggestTags() {
        $crit = new CDbCriteria();
        $term = Yii::app()->getRequest()->getParam('tag');
        $tags = array();
        if ($term) {
            $crit->condition = "name LIKE '" . $term . "%' ";
            $crit->order = 'name';
            $crit->limit = 50;

            foreach (Tag::model()->findAll($crit) as $tag) {
                $tags[] = array(
                    'id' => $tag->id,
                    'label' => CHtml::encode($tag->name),
                    'value' => CHtml::encode($tag->name),
                );
            }
        }

        echo CJSON::encode($tags);
    }
    
    public function actionSuggestAchs() {
        $crit = new CDbCriteria();
        $term = Yii::app()->getRequest()->getParam('q');
        if ($term)
            $crit->condition = "name LIKE '" . $term . "%' ";
        $crit->order = 'name';
        $crit->limit = 50;
        $achs = array();
        foreach (Achievement::model()->findAll($crit) as $ach) {
            $achs[] = array(
                'id' => $ach->id,
                'text' => 'id'.$ach->id . ': ' . CHtml::encode($ach->name),
            );
        }

        echo CJSON::encode($achs);
    }
    
    public function actionGetAchsByIds(){
        $ids = Yii::app()->getRequest()->getParam('ids');
        $achs = array();
        if ($ids) {
            foreach (Achievement::model()->findAllByAttributes(array('id' => explode(',', $ids))) as $ach) {
                $achs[] = array(
                    'id' => $ach->id,
                    'text' => 'id'.$ach->id . ': ' . CHtml::encode($ach->name),
                );
            }
        }

        echo CJSON::encode($achs);
    }
    
    public function actionLatestActions(){
        $criteria = new CDbCriteria();
        
        if (!empty($_REQUEST['latestId'])){
            $criteria->condition = 'id > :latest_id';
            $criteria->params = array(':latest_id' => (int)$_REQUEST['latestId']);
            $criteria->limit = 2;
        } else {
            $criteria->limit = 4;
        }
        $criteria->order = 'id DESC';
        
        $rows = array();
        $models = UserPoints::model()->findAll($criteria);
        if (count($models)){
            foreach ($models as $model){
                if (in_array($model->user->role, array('admin', 'moderator'))){
                    continue;
                }
                $row = new stdClass;
                $row->id = $model->id;
                $row->content = $this->renderPartial('latestActions', array('model'=>$model), true);
                $rows[] = $row;
            }
        }
        $rows = array_reverse($rows);
        echo CJSON::encode($rows);
    }
}