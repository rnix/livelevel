<?php

class DevController extends Controller {

    public function filters() {
        return array(
            'accessControl',
        );
    }
    
    public function actions() {
        return array(
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('EditApp'),
                'users' => array('@'),
            ),
            array('deny',
                'actions' => array('EditApp'),
            ),
        );
    }
    
    public function actionIndex(){
        
        $this->render('index');
    }
    
    public function actionEditApp($id = ''){
        $model = null;
        if ($id){
            $model = AppOAuth::model()->findByPk($id);
        }
        if (!$model){
            $model = new AppOAuth();
        }
        
        if (isset($_POST['AppOAuth'])){
            $model->attributes=$_POST['AppOAuth'];
            
            if($model->validate()){
                $model->app_owner_user_id = Yii::app()->user->id;
                $model->status = 1;
                $model->save();
                Yii::app()->user->setFlash('success', '<strong>Сохранено!</strong>');
                $this->redirect(array('dev/editApp', 'id'=>$model->client_id));
            }
        }
        
        $this->render('editApp', array(
            'model'=>$model,
        ));
    }
}