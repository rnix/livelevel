<?php

class OauthController extends Controller {

    public function actionAccess_token() {
        $oauth = MyYiiOAuth2::instance();
        echo $oauth->grantAccessToken();
    }

    public function actionAuthorize() {
        if (Yii::app()->user->isGuest) {
            Yii::app()->user->setState('oauth2LoginReturnUrl', Yii::app()->request->requestUri);
            $this->redirect('/login/index');
        } else {
            $oauth = MyYiiOAuth2::instance();
            $oauth->setVariable("user_id", Yii::app()->user->id);
            $auth_params = $oauth->getAuthorizeParams();
            $app = $oauth->getClients($auth_params['client_id']);

            if (Yii::app()->user->getModel()->checkGrantApp($auth_params['client_id'])){
                $oauth->finishClientAuthorization(TRUE, $auth_params);
            }
            
            if (!empty($_POST['auth_params'])) {  
                $postAuthParams = array_intersect_key(
                        $_POST['auth_params'], 
                        array(
                            'response_type' => '',
                            'client_id' => '',
                            'redirect_uri' => '',
                            'scope' => '',
                            'state' => '',
                ));
                if (isset($_POST['ok'])) {
                    
                    Yii::app()->user->getModel()->grantApp($auth_params['client_id']);
                    
                    $oauth->finishClientAuthorization(TRUE, $postAuthParams);
                } else if (isset($_POST['cancel'])) {
                    $oauth->finishClientAuthorization(FALSE, $postAuthParams);
                }
            }
            
            $this->render('authorize', array('app' => $app, 'auth_params' => $auth_params));
        }
    }

}