<?php

class UsersController extends Controller {

    public $userName;
    
    public function actionById($userId){
        $user = User::model()->find('id = :id', array(':id' => $userId));
        $this->actionView($user);
    }
    
    public function actionByName($userName){
        $user = User::model()->find('screen_name LIKE :userName', array(':userName' => $userName));
        $this->actionView($user);
    }

    public function actionView($user) {
        if ($user) {
            $this->userName = $user->screen_name;
            $criteria = new CDbCriteria();
            $criteria->condition = 'ach_approved_date IS NOT NULL AND status = ' . UserAchievement::STATUS_APPROVED;
            $achievedCount = Achievement::model()->achievedByUser($user)->public()->count($criteria);
            $achievedPages = new CPagination($achievedCount);
            $achievedPages->pageVar = 'achPage';
            $achievedPages->pageSize = 10;
            $achievedPages->params = $_GET;
            $criteria->order = 'ach_date DESC';
            unset($achievedPages->params['addedPage']);
            $achievedPages->applyLimit($criteria);
            $achievedList = Achievement::model()->achievedByUser($user)->public()->findAll($criteria);

            $criteria2 = new CDbCriteria();
            $addedCount = Achievement::model()->addedByUser($user)->public()->count($criteria2);
            $addedPages = new CPagination($addedCount);
            $addedPages->pageVar = 'addedPage';
            $addedPages->pageSize = 10;
            $addedPages->params = $_GET;
            $criteria2->order = 'ins_date DESC';
            unset($addedPages->params['achPage']);
            $addedPages->applyLimit($criteria2);
            $addedList = Achievement::model()->addedByUser($user)->public()->findAll($criteria2);

            $activeTab = 'achievedList';
            if (!empty($_GET['addedPage'])) {
                $activeTab = 'addedList';
            }

            $this->render('view', array(
                'user' => $user,
                'viewUser' => User::model()->findByPk(Yii::app()->user->getId()),
                'achievedList' => $achievedList,
                'achievedPages' => $achievedPages,
                'achievedCount' => $achievedCount,
                'addedList' => $addedList,
                'addedPages' => $addedPages,
                'addedCount' => $addedCount,
                'activeTab' => $activeTab,
            ));
        } else {
            throw new CHttpException(404, 'Такой пользователь не найден.');
        }
    }

    public function actionInstallRoles() {

        if (Yii::app()->user->getModel()->external_id_twi != '115622071'){
            throw new CHttpException(403,'Forbidden');
        }
        
        $auth = Yii::app()->authManager;

        //сбрасываем все существующие правила
        $auth->clearAll();
        
        $task = $auth->createTask('userManage', 'управление пользователями');
        $auth->createOperation('grantModeration', 'сделать модератором');
        $auth->createOperation('revokeModeration', 'отобрать модераторорство');
        $auth->createOperation('banUser', 'заблокировать пользователя');
        $task->addChild('grantModeration');
        $task->addChild('revokeModeration');
        $task->addChild('banUser');
        
        $task = $auth->createTask('approveAchieve', 'подтверждение достижения');
        $auth->createOperation('approve', 'принять');
        $auth->createOperation('reject', 'отклонить');
        $task->addChild('approve');
        $task->addChild('reject');
        
        $auth->createOperation('addAchievement', 'добавить достижение');
        $auth->createOperation('deleteAnyAchievement', 'удалить любое достижение');
        
        
        $bizRule='return Yii::app()->user->checkAccess("deleteAnyAchievement") 
            || (Yii::app()->user->id==$params["achievement"]->author_id && $params["achievement"]->isAbleToSafeDeleted());';
        $auth->createOperation('deleteAchievement', 'удалить достижение', $bizRule);
        
        $auth->createRole('guest');
        
        $role = $auth->createRole('user');
        $role->addChild('addAchievement');
        $role->addChild('deleteAchievement');
        
        $role = $auth->createRole('moderator');
        $role->addChild('user');
        $role->addChild('approveAchieve');
        
        $role = $auth->createRole('admin');
        $role->addChild('moderator');
        $role->addChild('userManage');
        $auth->createOperation('approveMyAchieve', 'подтверждать свои достижения');
        $role->addChild('approveMyAchieve');
        $role->addChild('deleteAnyAchievement');
        
        //связываем пользователя с ролью
        //$auth->assign('admin', 2);
        
        $auth->save();
        
        echo 'Done.';
    }
    
    public function actionTop(){
        $list = User::getTopList();
        
        $this->render('top', array(
            'users'=>$list
                ));
    }

    public function actionBan(){
        if (!Yii::app()->user->checkAccess('banUser')) {
            throw new CHttpException(403, 'Forbidden');
        }
        $model = User::model()->findByPk($_REQUEST['userid']);
        
        if ($model && !empty($_POST['confirm'])){
            $model->ban();
            $this->redirect(Yii::app()->homeUrl);
        }
        
        $this->render('confirmBan', array('model'=>$model));
    }
    
    public function actionBanned(){
        if (Yii::app()->user->getModel()->banned) {
            unset(Yii::app()->session['oauth_token'], Yii::app()->session['oauth_token_secret']);
            Yii::app()->user->logout();
            $this->render('banned');
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
    }
    
    public function actionApps(){
        if (!Yii::app()->user->isGuest){
            
            $models = Yii::app()->user->getModel()->userApps(
                    array('condition'=>'grant_status = :st', 
                        'params'=>array(':st'=>UserApp::GRANT_STATUS_GRANTED)
                        ));
            
            $myApps = AppOAuth::model()->findAll('app_owner_user_id=:uid', array(':uid'=>Yii::app()->user->getId()));
            
            $this->render('apps', array(
                'models'=>$models,
                'myApps'=>$myApps,
                ));
            
        } else {
            $this->redirect('/login/index');
        }
    }
    
    public function actionRevokeApp(){
        $appId = $_GET['appId'];
        if ($appId){
            Yii::app()->user->getModel()->revokeApp($appId);
            Yii::app()->user->setFlash('success', '<strong>Приложению убран доступ!</strong>.');
            $this->redirect(array('users/apps'));
        }
    }
}