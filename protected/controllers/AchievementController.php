<?php

class AchievementController extends Controller {

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'list'),
                'users' => array('@'),
            ),
            array('allow',
                'actions' => array('add'),
                'roles' => array('addAchievement'),
            ),
            array('deny',
                'actions' => array('add'),
            ),
        );
    }
    
    public function actionView($id){
        $ach = Achievement::model()->findByPk($id);
        if ($ach){
            $uaModel = new UserAchievement;
            $user = User::model()->findByPk(Yii::app()->user->getId());
            
            $criteria = new CDbCriteria();
            $criteria->condition = 'idach = :idach AND ach_approved_date IS NOT NULL AND status = ' . UserAchievement::STATUS_APPROVED;
            $criteria->params = array(':idach' => $ach->id);
            $criteria->order = 'ach_date';
            $achievedCount = UserAchievement::model()->count($criteria);
            $achievedPages = new CPagination($achievedCount);
            $achievedPages->pageSize = 10;
            $achievedPages->applyLimit($criteria);
            $achievedList = UserAchievement::model()->findAll($criteria);
            
            if ($user) {
                $criteria = new CDbCriteria();
                $criteria->condition = 'idach=:idach AND iduser=:iduser';
                $criteria->params = array(':idach' => $ach->id, ':iduser' => $user->id);
                $criteria->order = 'ins_date DESC';
                $achieveUserHistory = UserAchievement::model()->findAll($criteria);
            } else {
                $achieveUserHistory = array();
            }
            
            if (isset($_POST['UserAchievement']) && $user) {
                $uaModel->attributes = $_POST['UserAchievement'];
                if ($uaModel->validate() && $ach->userIsAbleToAchive($user)) {
                    $uaFirst = UserAchievement::model()->find('iduser=:iduser AND idach=:idach', array(':iduser' => $user->id, ':idach'=>$ach->id));

                    $uaModel->idach = $ach->id;
                    $uaModel->iduser = Yii::app()->user->getId();
                    $uaModel->save();
                    
                    if (!$uaFirst) {
                        $uaModel->user->addPoints(Yii::app()->params['achievePoints']['unproven'], UserPoints::ACTION_ACHIEVED, $ach->id);
                        if ($uaModel->iduser != $ach->author->id) {
                            $ach->author->addPoints(Yii::app()->params['achievePoints']['achieved_by_other'], UserPoints::ACTION_ACHIEVED_BY_OTHER, $ach->id);
                        }
                    }
                    
                    Yii::app()->user->setFlash('info', '<strong>Отправлено!</strong> Ваше достижение будет проверено модератором.');
                    $this->refresh();
                }
            } else {
                $uaModel->ach_date = date ('d.m.Y', time());
            }
            
            $this->render('view', array(
                'ach'=>$ach,
                'user' => $user,
                'uaModel'=>$uaModel,
                'achievedList'=>$achievedList,
                'achievedPages'=>$achievedPages,
                'achieveUserHistory'=>$achieveUserHistory,
            ));
        }
    }
    
    public function actionIndex() {
        return $this->actionList();
    }
    
    public function actionList() {
        $tags = Tag::model()->getPopular(10);
        $selectedTagsParam = Yii::app()->getRequest()->getParam('tags');
        if ($selectedTagsParam){
            $selectedTags = $selectedTagsParam;
            $achs = Achievement::model()->public()->withTags($selectedTagsParam)->findAll();
        } else {
            $selectedTags = array();
            $achs = Achievement::model()->public()->recently(15)->findAll();
        }
        
        $achList = $this->renderPartial('list', array(
            'achs' => $achs,
            'user' => User::model()->findByPk(Yii::app()->user->getId()),
                ), true);

        if (Yii::app()->request->isAjaxRequest) {
            echo $achList;
            Yii::app()->end();
        } else {
            $criteria = new CDbCriteria();
            $criteria->condition = 'iduser = :iduser AND ach_approved_date IS NOT NULL AND status IS NOT NULL';
            $criteria->params = array(':iduser' => Yii::app()->user->getId());
            $criteria->order = 'ins_date DESC';
            $criteria->limit = 5;
            $latestApprovementsList = UserAchievement::model()->findAll($criteria);
            
            $this->render('listPage', array(
                'achList' => $achList,
                'tags' => $tags,
                'selectedTags' => $selectedTags,
                'latestApprovementsList' => $latestApprovementsList,
            ));
        }
    }

    public function actionAdd() {
        if (Yii::app()->user->isGuest){
            $this->redirect(array('/'));
            return;
        }
        
        if (isset($_POST['DependsOn'])){
            $dependsOnPreselectedIds = $_POST['DependsOn'];
        } else {
            $dependsOnPreselectedIds = '';
        }
        
        $model = new Achievement;
        $lastUsedAppId = Yii::app()->user->getState('lastUsedAppId');
        if ($lastUsedAppId){
            $model->app_client_id = $lastUsedAppId;
        }
        
        $formTags = array();
        if (isset($_POST['Achievement']) && !Yii::app()->user->isGuest) {
            
            $model->public = 1;
            $model->attributes = $_POST['Achievement'];
            $model->author_id = Yii::app()->user->getId();
            $model->points = Yii::app()->params['achievePoints']['base'];
            
            $tags = Yii::app()->getRequest()->getParam('Tags');
            
            if ($tags) {
                $tagsList = array();
                foreach ($tags as $tagName) {
                    $tagName = Tag::cleanName($tagName);
                    $tag = Tag::model()->find('name LIKE :name', array(':name' => $tagName));
                    if (!$tag) {
                        $tag = new Tag;
                        $tag->author_id = Yii::app()->user->getId();
                        $tag->name = $tagName;
                        $tag->save();
                    }
                    if ($tag->name){
                        $tagsList[] = $tag;
                        $formTags[] = $tag->name;
                    }
                }
                $model->tags = $tagsList;
            }
            
            $depends = Yii::app()->getRequest()->getParam('DependsOn');
            if ($depends) {
                $model->achievementDependsOn = Achievement::model()->findAllByAttributes(array('id' => explode(',', $depends)));
            }
            
            if ($model->validate()) {
                $model->saveWithRelated(array('tags'));
                Yii::app()->user->setFlash('success', '<strong>Сохранено!</strong>');
                
                if (!empty($_POST['Achievement']['app_client_id'])){
                    Yii::app()->user->setState('lastUsedAppId', $model->app_client_id);
                } else {
                    Yii::app()->user->setState('lastUsedAppId', null);
                }
                
                $this->redirect('/id' . $model->id);
            }
        }
        $this->render('add', array(
            'model' => $model, 
            'dependsOnPreselectedIds' => $dependsOnPreselectedIds,
            'formTags' => $formTags,
            ));
    }
    
    public function actionApprove(){
        if (!Yii::app()->user->checkAccess('approveAchieve')){
            throw new CHttpException(403,'Forbidden');
        }
        
        if (!empty($_POST['uaIdActions'])) {
            $approveCount = 0;
            $rejectCount = 0;
            foreach ($_POST['uaIdActions'] as $uaId => $action) {
                $ua = UserAchievement::model()->findByPk($uaId);
                if ($ua && !$ua->ach_approved_date) {
                    if ($action == 'approve') {
                        $ua->approve(Yii::app()->user);
                        $approveCount++;
                    } else if ($action == 'reject') {
                        $comment = '';
                        if (!empty($_POST['rejectComments'][$uaId])){
                            $comment = $_POST['rejectComments'][$uaId];
                        }
                        $ua->reject(Yii::app()->user, $comment);
                        $rejectCount++;
                    }
                }
            }
            Yii::app()->user->setFlash('success', '<strong>Сохранено!</strong> Принято ' . $approveCount . ', отклонено '.$rejectCount . '.');
            $this->refresh();
        }
        
        $criteria = new CDbCriteria();
        $criteria->condition = 'ach_approved_date IS NULL AND iduser <> :iduser AND status IS NULL';
        $params = array();
        if (Yii::app()->user->checkAccess('approveMyAchieve')){
            $params[':iduser'] = 0;
        } else {
            $params[':iduser'] = Yii::app()->user->getId();
        }
        $criteria->params = $params;
        
        $criteria->order = 'ins_date';
        $count = UserAchievement::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 50;
        $pages->applyLimit($criteria);
        $userAchievements = UserAchievement::model()->findAll($criteria);

        $this->render('approve', array(
            'userAchievements' => $userAchievements
        ));
    }
    
    public function actionDelete(){
        $model = Achievement::model()->findByPk($_REQUEST['achid']);
        
        if (!Yii::app()->user->checkAccess('deleteAchievement', array('achievement'=>$model))) {
            throw new CHttpException(403, 'Forbidden');
        }
          
        if ($model && !empty($_POST['confirm'])){
            $usersToUpdate = array();
            foreach ($model->users as $ua){
                $usersToUpdate[] = $ua->user;
            }
            $model->delete();
            foreach ($usersToUpdate as $user){
                $user->updateCurrentLevel();
            }
            $this->redirect(Yii::app()->homeUrl);
        }
        
        $this->render('confirmDelete', array('model'=>$model));
    }
    
}