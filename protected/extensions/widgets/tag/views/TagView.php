<?php
$tag_it=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('ext.widgets.tag').'/tag-it.js');
$tag_it_css=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('ext.widgets.tag').'/tag-it.css');
$cs=Yii::app()->clientScript;
$cs->registerCoreScript('jquery.ui');
$cs->registerScriptFile($tag_it);
$cs->registerCssFile($tag_it_css);

$cs->registerScript($id,'
    $("#'.$id.'").tagit({
        tags: '.$tags.',
        url: "'.$url.'"
    });
', CClientScript::POS_READY);

?>

<div class="tagit-container">
    <label for="<?php echo CHtml::encode($id); ?>"><?= $label ?></label>
    <ul id="<?php echo CHtml::encode($id); ?>">
        <li class="tagit-new">
            <input class="tagit-input" type="text" />
        </li>
    </ul>
    <div class="tagit-hint">
        <em>Введите список тегов через запятую.</em>
    </div>
</div>