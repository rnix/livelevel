<?php
return array(
	'No matches found' => 'Ничего не найдено',
	'Please enter {chars} more characters' => 'Пожалуйста, введите символов более, чем {chars}',
	'Please enter {chars} less characters' => 'Пожалуйста, введите символов менее, чем {chars}',
	'You can only select {count} items' => 'Вы можете выбрать пунктов не более, чем {count}',
	'Loading more results...' => 'Загрузка результатов...',
	'Searching...' => 'Поиск...',
);
