<?php 
/* @var $this OAuth2Controller */
?>

<div class="span7">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm'); ?>

    <h4>Разрешить приложению <?= CHtml::encode($app['app_title']) ?> доступ к вашим данным?</h4>

    <?php
    foreach ($auth_params as $p => $v) {
        echo CHtml::hiddenField('auth_params[' . $p . ']', $v);
    }
    ?>

    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Разрешить', 'htmlOptions' => array('class' => 'btn-primary', 'name'=>'ok'))); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Отклонить', 'htmlOptions' => array('name'=>'cancel'))); ?>
    <?php $this->endWidget(); ?>
</div>