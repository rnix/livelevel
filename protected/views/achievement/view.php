<?php
/* @var $ach Achievement */
/* @var $user User */
/* @var $uaModel UserAchievement */
/* @var $achievedList UserAchievement[] */

$this->pageTitle = Yii::app()->name . ' - ' . CHtml::encode($ach->name);
$this->breadcrumbs = array(
    'Просмотр достижения'
);
?>
<?php $this->widget('widgets.achievement.AchWidget', array('model' => $ach, 'user' => $user, 'dependsCollapsed' => false)); ?>

<?php if ($user) { ?>

    <?php
    $this->widget('bootstrap.widgets.TbAlert', array(
        'block' => true,
        'fade' => true,
        'closeText' => '&times;',
        'alerts' => array('info' => array('block' => true, 'fade' => true, 'closeText' => '&times;')),
    ));

    $ua = $ach->getUserAchievement($user);
    if ($ach->userIsAbleToAchive($user)) {
        ?>
        <button type="button" class="btn" data-toggle="collapse" data-target="#achieveFormContainer">
            Да, я достиг его
        </button>
        <div id="achieveFormContainer" class="collapse">
            <?php
            $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id' => 'achieve-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
                'htmlOptions' => array('class' => 'well'),
                    ));
            ?>

            <?php echo $form->errorSummary($uaModel); ?>

            <div>
                <label for="UserAchievement_ach_date" class="required">
                    Дата достижения
                    <span class="required">*</span></label>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'language' => Yii::app()->getLanguage(),
                    'model' => $uaModel,
                    'attribute' => 'ach_date',
                ));
                ?>
                <?php echo $form->textFieldRow($uaModel, 'proof_link1', array('class' => 'span4')); ?>
                <?php echo $form->textFieldRow($uaModel, 'proof_link2', array('class' => 'span4')); ?>
                <p>
                    Если подтверждающая ссылка ведёт на какой-либо ваш профиль (vk.com, facebook.com, battle.net и т. п.),
                    то для того, чтобы доказать, что это профиль принадлежит вам, от имени этого профиля должно быть написано сообщение/статус/комментарий,
                    содержащее текст <br>
                    либо <em><?= $user->getHash() ?></em>, <br>
                    либо <em>livelevel.net/user/<?=$user->id?>/<?= CHtml::encode($user->screen_name) ?></em>.
                </p>
            </div>

            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Отправить на проверку', 'htmlOptions' => array('class' => 'btn-primary'))); ?>

            <?php $this->endWidget(); ?>
        </div>
    <?php } else if ($ua) { ?>
        <div class="well">
            Достигнуто вами <?= CHtml::encode($ua->ach_date); ?>.
            <?php if ($ua->proof_link1) { ?>
                <div>
                    С поддтверждающей ссылкой <?= CHtml::encode($ua->proof_link1) ?><?php if ($ua->proof_link2) { ?> и с <?= CHtml::encode($ua->proof_link2) ?><?php } ?>.
                </div>
            <?php } ?>
            <div>
                Получено очков: <?= (int) $ua->countPoints(); ?>.
            </div>
            <?php if ($ua->ach_approved_date && $ua->status == UserAchievement::STATUS_APPROVED) { ?>
                <?php
                $this->widget('bootstrap.widgets.TbLabel', array(
                    'type' => 'success',
                    'label' => 'Подтверждено',
                ));
                ?>
            <?php } else { ?>
                <?php
                $this->widget('bootstrap.widgets.TbLabel', array(
                    'label' => 'Не подтверждено',
                ));
                ?>
            <?php } ?>
        </div>
    <?php } else { ?>
        <div class="alert alert-danger">
            Вы не можете выполнить это достижение в данный момент.
            <?php if ($ach->app_client_id) { ?>
                Достижение управляется приложением <a href="<?= Yii::app()->createUrl('/app/' . $ach->app_client_id) ?>"><?= CHtml::encode($ach->app->app_title) ?></a>.
            <?php } ?>
        </div>
    <?php } ?>
<?php } else { ?>
    <div class="alert alert-info">
        Войдите, чтобы узнать состояние этого достижения.
    </div>
<?php } ?>

<?php if (!$ach->app_client_id) { ?>
    <h4>Ваша персональная история этого достижения</h4>
    <?php if (count($achieveUserHistory)) { ?>
        <table class="table">
            <tr>
                <th>#</th>
                <th>Дата записи</th>
                <th>Дата достижения</th>
                <th>Подтверждающие ссылки</th>
                <th>Статус</th>
                <th>Комментарий</th>
            </tr>

            <?php
            $i = 0;
            foreach ($achieveUserHistory as $ua) {
                $i++;
                ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?php echo Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($ua->ins_date), 'medium', null); ?></td>
                    <td><?php echo $ua->ach_date; ?></td>
                    <td>
                        <?php
                        $links = array();
                        if ($ua->proof_link1) {
                            $links[] = CHtml::link(CHtml::encode($ua->proof_link1), CHtml::encode($ua->proof_link1), array('target' => '_blank'));
                        }
                        if ($ua->proof_link2) {
                            $links[] = CHtml::link(CHtml::encode($ua->proof_link2), CHtml::encode($ua->proof_link2), array('target' => '_blank'));
                        }
                        if ($links) {
                            echo implode('<br>', $links);
                        } else {
                            echo '<em>Нет ссылок.</em>';
                        }
                        ?>
                    </td>
                    <td>
                        <?php if ($ua->status == UserAchievement::STATUS_APPROVED) { ?>
                            <i class="icon-ok" title="Принято"></i>
                        <?php } else if ($ua->status == UserAchievement::STATUS_REJECTED) { ?>
                            <i class="icon-remove" title="Отклонено"></i>
                        <?php } else { ?>
                            <i class="icon-time" title="Ожидает"></i>
            <?php } ?>
                    </td>
                    <td><?php if ($ua->comment) echo CHtml::encode($ua->comment); else echo '-'; ?></td>
                </tr>
        <?php } ?>
        </table>
    <?php } else { ?>
        <em>Вы не подавали заявку на подтверждение этого достижения.</em>
    <?php } ?>
<?php } ?>

<h4>Список пользователей, которые достигли этого</h4>
<?php if (count($achievedList)) { ?>

    <table class="table">
        <tr>
            <th>#</th>
            <th>Пользователь</th>
            <th>Дата достижения</th>
        </tr>
    <?php $i = 0;
    foreach ($achievedList as $ach) {
        $i++; ?>
            <tr>
                <td><?= $i ?></td>
                <td><a href="<?= $ach->user->getProfileUrl() ?>"><?= CHtml::encode($ach->user->screen_name) ?></a></td>
                <td><?= $ach->ach_date ?></td>
            </tr>
    <?php } ?>
    </table>
    <?php
    $this->widget('CLinkPager', array(
        'pages' => $achievedPages,
    ))
    ?>
<? } else { ?>
    <em>Таких ещё нет. Будьте первыми!</em>
<?php } ?>