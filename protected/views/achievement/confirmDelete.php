<?php 
/* @var $model Achievement */
?>
<h2>Действительно удалить?</h2>
<?php $this->beginWidget('bootstrap.widgets.TbActiveForm'); ?>
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Да, удалить', 'htmlOptions' => array('class' => 'btn-danger', 'name'=>'confirm', 'value'=>'yes'))); ?>
<?php $this->endWidget(); ?>


<?php $this->widget('widgets.achievement.AchWidget', array('model' => $model)); ?>
