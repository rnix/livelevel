<?php
/* @var $userAchievements UserAchievement[] */
$this->pageTitle = Yii::app()->name . '- Модерирование';
?>
<h4>Список для подтверждения</h4>

<?php
$this->widget('bootstrap.widgets.TbAlert', array(
    'alerts' => array('success' => array('block' => true, 'fade' => true, 'closeText' => '&times;')),
));
?>

<div class="alert alert-info">
    Достижение следует принимать, если ссылки содержат:
    <ol>
        <li>подтверждение того, что отмеченное достижение выполнено полностью;</li>
        <li>подтверждение того, что достижение выполнил именно указанный пользователь.</li>
    </ol>
    Если же нет оснований полагать, что эти два условия выполнены, то в подтверждении
    следует отказать.
</div>
<?php if (count($userAchievements)) { ?>
    <h6>найдено: <?= count($userAchievements) ?></h6>
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm'); ?>
    <table class="table">
        <tr>
            <th>#</th>
            <th>Пользователь</th>
            <th>Достижение</th>
            <th>Дата записи</th>
            <th>Дата достижения</th>
            <th>Подтверждающие ссылки</th>
            <th></th>
            <th><i class="icon-ok"></i></th>
            <th><i class="icon-remove"></i></th>
            <th>Комментарий</th>
        </tr>

        <?php $i = 0;
        foreach ($userAchievements as $ua) {
            $i++; ?>
            <tr>
                <td><?= $i ?></td>
                <td><?php $this->widget('widgets.user.UserWidget', array( 'model' => $ua->user));?></td>
                <td><?php $this->widget('widgets.achievement.ShortWidget', array( 'model' => $ua->ach));?></td>
                <td><?php echo Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($ua->ins_date), 'medium', null); ?></td>
                <td><?php echo $ua->ach_date; ?></td>
                <td>
                    <?php 
                    $links = array();
                    if ($ua->proof_link1){
                        $links[] = CHtml::link(CHtml::encode($ua->proof_link1), CHtml::encode($ua->proof_link1), array('target'=>'_blank'));
                    }
                    if ($ua->proof_link2){
                        $links[] = CHtml::link(CHtml::encode($ua->proof_link2), CHtml::encode($ua->proof_link2), array('target'=>'_blank'));
                    }
                    if ($links){
                        echo implode('<br>', $links);
                    } else {
                        echo '<em>Нет ссылок.</em>';
                    }
                    ?>
                    <div title="Хеш пользователя"><?=$ua->user->getHash()?></div>
                </td>
                <td><input type="radio" name="uaIdActions[<?=$ua->id?>]" value="" title="Сбросить" checked="checked"></td>
                <td><input type="radio" name="uaIdActions[<?=$ua->id?>]" value="approve" title="Принять"></td>
                <td><input type="radio" name="uaIdActions[<?=$ua->id?>]" value="reject" title="Отклонить"></td>
                <td><textarea name="rejectComments[<?=$ua->id?>]" placeholder="Причина отказа"></textarea></td>
            </tr>
    <?php } ?>
    </table>
    <div class="well">
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Применить', 'htmlOptions' => array('class' => 'btn-primary'))); ?>
    </div>
    <?php $this->endWidget(); ?>
<?php } else { ?>
    <em>ничего не найдено.</em>
<?php } ?>
