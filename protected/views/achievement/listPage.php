<?php
$this->pageTitle = Yii::app()->name . ' - Достижения';
?>
<div class="row">
    <div class="span3">
        <form>
            <input type="text" class="search-query" id="search-tags-input" placeholder="Поиск">
            <div class="clearfix"></div>

            <div class="tags-list">
                <em>теги:</em>
                <em id="search-tags-not-found" style="display: none;">Такие теги не найдены.</em>
                <ul>
                    <?php
                    $i = 0;
                    foreach ($tags as $tag) {
                        $i++;
                        if (in_array($tag->name, $selectedTags)) {
                            $checked = 'active';
                        } else {
                            $checked = '';
                        }
                        ?>
                        <li class="search-tag">
                            <a href="#" class="tag btn <?= $checked ?>" data-tag="<?= CHtml::encode($tag->name) ?>">
                                <?= CHtml::encode($tag->name) ?> 
                                <i class="icon-remove"></i>
                            </a>
                        </li>

                    <?php } ?>
                    <li class="search-tag" id="search-tag-tpl" style="display: none;">
                        <a href="#" class="btn tag" data-tag="">
                            <i class="icon-remove"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </form>
        
        <h5>Соседние проекты</h5>
        <ul>
            <li><a href="http://tronode.livelevel.net">Tronode.js</a> - игра, клон Трона с мотоциклами.</li>
            <li><a href="http://wd.getid.org">Web Developer</a> - игра-кликер о веб-программировании.</li>
            <li><a href="http://qgame.livelevel.net">Очередь</a> - игра в очередь.</li>
            <li><a href="http://amoeba.livelevel.net">Амёба</a> - игра, гонки с огибанием препядствий.</li>
            <li><a href="http://battleship.livelevel.net">Морской бой</a> - онлайн игра в морской бой против друга.</li>
            <li><a href="http://checkers.livelevel.net">Шашки</a> - онлайн игра в шашки против друга.</li>
            <li><a href="http://maze.livelevel.net">Лабиринт</a> - онлайн игра по прохождению лабиринта одному или против других посетителей.</li>
        </ul>
    </div>

    <div class="span6">
        <div class="well selected-tags-list">
            <ul>
                <?php foreach ($selectedTags as $sTagName) { ?>
                    <li class="tag btn" data-tag="<?= CHtml::encode($sTagName) ?>"><?= CHtml::encode($sTagName) ?> <i class="icon-remove"></i></li>
                <?php } ?>
                <li class="tag btn" data-tag="" style="display: none;" id="selected-tag-tpl"> <i class="icon-remove"></i></li>
            </ul>
        </div>
        <div class="ach-list">
            <?= $achList; ?>
        </div>
    </div>
    
    <div class="span3">
        <?php if (count($latestApprovementsList)) { ?>
        <h5>Последние подтверждения ваших достижений</h5>
        <ul class="latest-approvements-list">
            <?php foreach ($latestApprovementsList as $ua){ ?>
                <li>
                    <?php if ($ua->status == UserAchievement::STATUS_APPROVED) { ?>
                        Достижение <?php $this->widget('widgets.achievement.ShortWidget', array( 'model' => $ua->ach));?> было <span class="label label-success">подтверждено</span> модератором.
                    <?php } else if ($ua->status == UserAchievement::STATUS_REJECTED) { ?>
                        Достижение <?php $this->widget('widgets.achievement.ShortWidget', array( 'model' => $ua->ach));?> было <span class="label label-important">отклонено</span> модератором.
                        <?php if ($ua->comment) { ?>
                            <div>С комментарием: <em><?=CHtml::encode($ua->comment);?></em></div>
                        <?php } ?>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
        <?php } ?>
        
        <h5>Последние действия</h5>
        <ul class="latest-actions-list">
        </ul>
    </div>
</div>
