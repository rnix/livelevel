<?php
/* @var $this AchievementController */
/* @var $model Achievement */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - Добавить достижение';
$this->breadcrumbs = array(
    'Добавить достижение'
);
?>

<?php if (Yii::app()->user->hasFlash('success')): ?>

    <?php
    $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts' => array('success' => array('block' => true, 'fade' => true, 'closeText' => '&times;')),
    ));
?>

<?php else: ?>


    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'achievement-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'htmlOptions' => array('class' => 'well'),
            ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <div>
        <?php echo $form->textFieldRow($model, 'name', array('class' => 'span6')); ?>
        <?php echo $form->textareaRow($model, 'desc', array('class' => 'span5', 'rows' => 6, 'cols' => 50)); ?>
        <div class="ach-form-desc"><em>В описании можете указать, как следует подтверждать это достижение.</em></div>

        <?php
        $this->widget('ext.widgets.tag.TagWidget', array(
            'label' => 'Теги (обязательно)',
            'url' => Yii::app()->request->baseUrl . '/json/suggestTags',
            'tags' => $formTags,
            'id' => 'tags-list',
        ));
        ?>
        
        <label class="required" for="s2id_DependsOn">Укажите достижения, которые предварительно нужно выполнить (необязательно)</label>
            <?php
            echo CHtml::textField("DependsOn", $dependsOnPreselectedIds, array('class'=>'span5'));
            $this->widget('widgets.DependedAchs', array(
                'selector' => "#DependsOn",
                'preselectedIds' => $dependsOnPreselectedIds,
            ));
            ?>
        <div style="clear: both;"></div>
        
        
            <?php if (count(Yii::app()->user->getModel()->ownedApps(array('condition' => 'status=1')))) { ?>
                <div>
                    <label for="Achievement_app_client_id">Достижение будет выполняться через приложение</label>
                    <?php
                    $options = array('' => 'нет');
                    foreach (Yii::app()->user->getModel()->ownedApps(array('condition' => 'status=1')) as $app) {
                        $options[$app->client_id] = CHtml::encode($app->app_title) . ' (' . $app->client_id . ')';
                    }
                    echo $form->dropDownList($model, 'app_client_id', $options);
                    ?>

                </div>

                <?php if (!$model->id) { ?>
                    <div>
                        <?php echo $form->checkBox($model, 'public', array('checked' => 'checked')); ?>
                        <label for="Achievement_public">Показывать в общем списке (вы не сможете изменить это значение в дальнейшем)</label>
                    </div>
                <?php } ?>
            <?php } ?>
        
        
        
        
    </div>

    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Добавить', 'htmlOptions' => array('class' => 'btn-primary'))); ?>

    <?php $this->endWidget(); ?>


<?php endif; ?>