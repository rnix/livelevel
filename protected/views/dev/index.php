<?php 
/* @var $this DevController */
$this->pageTitle = Yii::app()->name . ' - Разработчикам';
?>

<h2>Разработчикам</h2>

<p>LiveLevel предоставляет возможность подключить ваш сервис/приложение/игру для автоматического учёта достижений пользователя.
При этом вы сами создаете любые достижения. Взаимодействие происходит через API, а подключение пользователя происходит по протоколу OAuth v2.</p>

<h4>Для этого необходимо:</h4>
<ol>
    <li><a href="<?=$this->createUrl('dev/editApp')?>">Создать приложение</a> в LiveLevel.</li>
    <li><a href="<?=$this->createUrl('achievement/add')?>">Создать достижения</a> для вашего приложения в LiveLevel.</li>
    <li><a href="<?=$this->createUrl('dev/page/view/oauth_help')?>">Подключить пользователя</a> по протоколу OAuth v2 к вашему сервису/приложению/игре.</li>
    <li><a href="<?=$this->createUrl('dev/page/view/api_help')?>">Использовать API</a> для назначения достижений пользователю.</li>
</ol>

<h4>Пример</h4>
В открытом доступе находятся исходные коды игры <a href="http://maze.livelevel.net">Maze</a>. Смотреть здесь: <a href="https://bitbucket.org/rnix/maze" target="_blank">https://bitbucket.org/rnix/maze</a>.
Игра maze содержит класс protected/components/LivelevelOAuthService, который используется расширением eauth для аутентификации пользователя.
Для вызова методов LiveLevel API используется класс protected/components/LiveLevelApi.