<?php $this->pageTitle = Yii::app()->name . ' - Авторизация сайтов'; ?>
<h3>Авторизация сайтов</h3>

<p>
    Механизм авторизации на базе протокола OAuth 2.0 стандартный и отличается от <a href="https://vk.com/developers.php?oid=-1&p=%D0%90%D0%B2%D1%82%D0%BE%D1%80%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F_%D1%81%D0%B0%D0%B9%D1%82%D0%BE%D0%B2">авторизации сайтов в ВКонакте</a> лишь некоторыми параметрами в запросе.
    <br>
    Отличия в отсутствии параметра <i>scope</i> для 1-го пункта и необходимости <i>grant_type=authorization_code</i> для 4-го пункта.
</p>

<p>
Процесс авторизации сайта состоит из 4-х шагов:</p>

<ol class="numbered">
	<li><span>Открытие окна браузера для аутентификации пользователя на сайте LiveLevel. </span></li>
	<li><span>Разрешение пользователем доступа к своим данным. </span></li>
	<li><span>Передача сайту значения <b>code</b> для получения ключа доступа. </span></li>
	<li><span>Получение сервером приложения ключа доступа <b>access_token</b> для доступа к API LiveLevel. </span></li>
</ol>

<p><br />
<a name="1. Открытие диалога авторизации OAuth"></a></p>

<h4>1. Открытие диалога авторизации OAuth</h4>

<p>Для начала процесса авторизации необходимо создать окно браузера и открыть в нём диалог авторизации с параметром <b>response_type = code</b> по адресу:</p>

<blockquote><code><a href="http://livelevel.net/oauth/authorize">http://livelevel.net/oauth/authorize</a>?<br />
&nbsp;client_id=<b>APP_ID</b>&amp;<br />
&nbsp;redirect_uri=<b>REDIRECT_URI</b>&amp;<br />
&nbsp;response_type=code</code></blockquote>

<p><br />
APP_ID &ndash; идентификатор Вашего приложения;<br />
REDIRECT_URI &ndash; адрес, на который будет передан <b>code</b>. Этот адрес должен находиться в пределах домена, указанного в настройках приложения.<br />
<br />
Если пользователь не вошел на сайт, то в диалоговом окне ему будет предложено это сделать.</p>


<p><br />
<a name="2. Разрешение прав доступа"></a></p>

<h4>2. Разрешение прав доступа</h4>

<p>После успешного входа на сайт пользователю будет предложено авторизовать приложение.</p>


<p><br />
<a name="3. Получение code"></a></p>

<h4>3. Получение code</h4>

<p>После успешной авторизации приложения браузер пользователя будет перенаправлен по адресу REDIRECT_URI, указанному при открытии диалога авторизации. При этом код для получения ключа доступа <b>code</b> будет передан в GET-параметре на указанный адрес:<br />
&nbsp;</p>

<blockquote><code>http://REDIRECT_URI?code=7a6fa4dff77a228eeda56603b8f53806c883f011c40b72630bb50df056f6479e52a</code></blockquote>

<p><br />
<br />
Параметр <b>code</b> может быть использован в течение 1 часа для получения ключа доступа к API <b>access_token</b> с Вашего сервера.<br />
<br />
В случае возникновения <b>ошибки</b> браузер пользователя будет перенаправлен с кодом и описанием ошибки.<br />
Пример возвращаемой ошибки:<br />
&nbsp;</p>

<blockquote><code>http://REDIRECT_URI?error=invalid_request&amp;error_description=Invalid+display+parameter</code></blockquote>

<p><br />
<br />
<a name="4. Получение access_token"></a></p>

<h4>4. Получение access_token</h4>

<p><br />
Для получения <b>access_token</b> необходимо выполнить запрос с Вашего сервера на URL <a href="http://livelevel.net/oauth/access_token">http://livelevel.net/oauth/access_token</a> с передачей параметра <b>code</b> и секретных данных приложения: client_id и client_secret. Секретный ключ приложения client_secret может быть получен в разделе редактирования приложения.<br />
&nbsp;</p>

<blockquote><code><a href="http://livelevel.net/oauth/access_token">http://livelevel.net/oauth/access_token</a>?<br />
client_id=<b>APP_ID</b>&amp;<br />
client_secret=<b>APP_SECRET</b>&amp;<br />
code=7a6fa4dff77a228eeda56603b8f53806c883f011c40b72630bb50df056f6479e52a&amp;<br />
grant_type=authorization_code&amp;<br />
redirect_uri=<b>REDIRECT_URI</b></code></blockquote>

<p><br />
Обратите внимание, что&nbsp;<b>REDIRECT_URI</b> должен быть тот-же, что и в пункте 1.<br />
<br />
В результате выполнения данного запроса Ваш сервер получит вновь созданный <b>access_token</b>. Вместе с access_token серверу возвращается время жизни ключа <b>expires_in</b> в секундах. Процедуру авторизации приложения необходимо повторять в случае истечения срока действия access_token.<br />
<br />
Пример ответа сервера:</p>

<blockquote><code>{&quot;access_token&quot;:&quot;533bacf01e11f55b536a565b57531ac114461ae8736d6506a3&quot;, &quot;expires_in&quot;:43200}</code></blockquote>

<p><br />
<br />
В случае ошибки будут переданы параметры <b>error</b> и <b>error_description</b>.</p>

<blockquote>
<code>
{&quot;error&quot;:&quot;invalid_grant&quot;,&quot;error_description&quot;:&quot;Code is expired.&quot;}</code>
</blockquote>

<p><br />
<br />
После успешной авторизации Вы можете <a href="<?=$this->createUrl('dev/page/view/api_help')?>">осуществлять запросы к API</a>.</p>
