<?php $this->pageTitle = Yii::app()->name . ' - Описание API'; ?>
<link rel="stylesheet" type="text/css" href="<?=$this->assetsCssBase?>/api-docs.css" />

<h3>Описание API</h3>
<p><em>API в разработке, этот раздел будет пополняться и обновляться. Пожелания добавления новых методов можете написать в Твиттере <a href="https://twitter.com/LiveLevelNet" target="_blank">@LivelevelNet</a>.</em></p>
<div class="api-docs">
    <?php
    $this->renderPartial('apiSection', array(
        'name' => 'Достижения',
        'desc' => 'Достижения - основной объект сервиса.',
        'methods' => array(
            array(
                'http_method' => 'GET',
                'uri' => 'achievements/view/:id',
                'desc' => 'Возвращает информацию о достижении.'
                ),
            array(
                'http_method' => 'GET',
                'uri' => 'achievements/owned_by_app',
                'desc' => 'Возвращает cписок достижений, которые управляются текущим приложением.'
                ),
            array(
                'http_method' => 'POST',
                'uri' => 'achievements/achieve/:id',
                'desc' => 'Добавляет (разблокирует) текущему пользователю достижение с отметкой подтверждения приложением с текущей датой. Достижение должно управляться приложением. При этом пользователю начисляются очки.'
                ),
            array(
                'http_method' => 'POST',
                'uri' => 'achievements/fail/:id',
                'desc' => 'Отменяет (блокирует) текущему пользователю достижение. При этом у пользователя удаляются соответствующие очки.'
                ),
    )));
    ?>
</div>