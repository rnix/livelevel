<?php 
/* @var $this DevController */
$this->pageTitle = Yii::app()->name . ' -Приложения';
?>

<?php if ($model->client_id) { ?>
    <h2>Редактирование приложения</h2>
<?php } else { ?>
    <h2>Создание приложения</h2>
<?php } ?>

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'app-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array('class' => 'well'),
        ));
?>

<?php
$this->widget('bootstrap.widgets.TbAlert', array(
'alerts' => array('success' => array('block' => true, 'fade' => true, 'closeText' => '&times;')),
));
?>

<?php echo $form->errorSummary($model); ?>
<p class="note"><em>Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</em></p>


<div>
    <?php echo $form->textFieldRow($model, 'app_title', array('class' => 'span6')); ?>

    <?php echo $form->textareaRow($model, 'app_desc', array('class' => 'span5', 'rows' => 6, 'cols' => 50)); ?>
    <?php echo $form->textFieldRow($model, 'redirect_uri', array('class' => 'span6')); ?>

    <?php if ($model->client_id) { ?>
        <p>ID приложения <br><strong><?=$model->client_id?></strong></p>
        <?php echo $form->textFieldRow($model, 'client_secret', array('class' => 'span6')); ?>
    <?php } ?>
    <div class="clear-fix"></div>
</div>

<?php if ($model->client_id) { ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Сохранить изменения', 'htmlOptions' => array('class' => 'btn-primary'))); ?>
<?php } else { ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Добавить приложение', 'htmlOptions' => array('class' => 'btn-primary'))); ?>
<?php } ?>
<?php $this->endWidget(); ?>