<table class="table">
    <caption><strong><?= $name ?></strong><p><?= $desc ?></p></caption>
    <thead>
        <tr>
            <th class="views-field views-field-title">
                Ресурс        </th>
            <th class="views-field views-field-body">
                Описание        </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($methods as $method) { ?>
            <tr class="odd views-row-first">
                <td class="views-field views-field-title"><?= $method['http_method'] ?> <?= $method['uri'] ?></td>
                <td class="views-field views-field-body"><?= $method['desc'] ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>