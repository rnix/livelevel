<?php
$this->pageTitle = Yii::app()->name . ' - Приложение - ' . CHtml::encode($app->app_title);
?>

<div class="span7">

    <div class="well">
        <h2>Приложение <?= CHtml::encode($app->app_title) ?></h2>
        <p><?= CHtml::encode($app->app_desc) ?></p>
        <p>Адрес: <a href="<?= CHtml::encode($app->redirect_uri) ?>"><?= CHtml::encode($app->redirect_uri) ?></a></p>
    </div>


    <?php if (count($achs)) { ?>
        <h4>Достижений, управляемых этим прилоежнием: <?= count($achs) ?></h4>
        <?php foreach ($achs as $ach) { ?>
            <?php $this->widget('widgets.achievement.AchWidget', array('model' => $ach, 'user' => $user)); ?>
        <?php } ?>
    <?php } else { ?>
        <em>ничего не найдено.</em>
    <?php } ?>

</div>