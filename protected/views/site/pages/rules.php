<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Правила';
$this->breadcrumbs=array(
	'Правила',
);
?>
<?php if (Yii::app()->user->isGuest) { ?>
    <h3>Здравствуй, гость</h3>
<?php } else { ?>
    <h3>Здравствуй, <?=CHtml::encode(Yii::app()->user->name)?></h3>
<?php } ?>


<p>Я хочу сыграть в маленькую игру.</p>
<p>Большинство людей в нашем мире абсолютно не ценит жизнь. Но только не вы. И не теперь.</p>

<p>
    Сообщая о каждом вашем достижении, вы получаете <strong><?=Yii::app()->params['achievePoints']['unproven']?></strong> очко. 
    Предъявляя доказательство достижения, вы получаете <strong><?=Yii::app()->params['achievePoints']['base']?></strong> очков.<br>
    Вы можете добавить в каталог свои собственные достижения. 
    Вы получаете <strong><?=Yii::app()->params['achievePoints']['achieved_by_other']?></strong> очко, если пользователь укажет достижение, которое добавили вы. 
    <br>
    Если он его докажет, вы получаете еще <strong><?=Yii::app()->params['achievePoints']['achieved_and_proved_by_other']?></strong> очка.
</p>

<p>Покажите, чего вы достигли в этой жизни.</p>

<div class="rules-saw visible-desktop">
    &nbsp;
</div>