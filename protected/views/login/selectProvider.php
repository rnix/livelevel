<?php
/* @var $this LoginController */
?>
<div class="row">

    <?php if ($isGuest || $canConnectTwi) { ?>
        <div class="span2">
            <a href="<?= Yii::app()->createUrl('login/twi') ?>" class="btn btn-primary">
                <?php if ($canConnectTwi) { ?> Подключить <?php } else { ?>Войти через<?php } ?><br>
                <strong>Twitter</strong>
            </a>
        </div>
    <?php } ?>

    <?php if ($isGuest || $canConnectVk) { ?>
        <div class="span2">
            <a href="<?= Yii::app()->createUrl('login/vk') ?>" class="btn btn-primary">
                <?php if ($canConnectVk) { ?> Подключить <?php } else { ?>Войти через<?php } ?><br>
                <strong>VK</strong>
            </a>
        </div>
    <?php } ?>

    <?php if (!$isGuest && !$canConnectTwi && !$canConnectVk) { ?>
        <div class="alert alert-info">Вы уже аутентифицированы и все сервисы подключены.</div>
    <?php } ?>
</div>