<!DOCTYPE html>  
<html>  
    <head>  
        <meta charset="utf-8">  
        <title><?php echo CHtml::encode($this->pageTitle); ?></title> 
        <?php Yii::app()->bootstrap->register(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <link rel="shortcut icon" href="/images/favicon.ico">
        <meta name="description" content="Как много вы добились?">        
        <link rel="stylesheet" type="text/css" href="<?=$this->assetsCssBase?>/main.css" />
        <?Yii::app()->clientScript->registerScriptFile($this->assetsJsBase.'/main.js')?>
    </head>  
    <body>  
        <div class="navbar navbar-fixed-top title-nav">  
            <div class="navbar-inner">  
                <div class="container">  
                    <a class="brand" href="<?=Yii::app()->homeUrl?>"><?php echo CHtml::encode(Yii::app()->name); ?></a>  
                    <div>  
                        <?php
                        $mainMenuItems = array(
                            array('label' => 'Достижения', 'url' => array('/achievement/index')),
                            array('label' => 'Добавить достижение', 'url' => array('/achievement/add')),
                            array('label' => 'Топ', 'url' => array('/users/top')),
                        );
                        if (!Yii::app()->user->isGuest){
                            $active = false;
                            if ($this->id == 'users' && Yii::app()->user->name == $this->userName){
                                $active = true;
                            }
                            $mainMenuItems[] = array('label' => 'Мой профиль', 'url' => array('/user/' . Yii::app()->user->getModel()->screen_name), 'active'=>$active);
                        }
                        
                        if (Yii::app()->user->checkAccess('approveAchieve')){
                            $count = UserAchievement::countForApprove();
                            if ($count){
                                $countLabel = " ($count)";
                            } else {
                                $countLabel = '';
                            }
                            $mainMenuItems[] = array('label' => 'Модерирование'.$countLabel, 'url' => array('/achievement/approve'));
                        }
                        
                        $this->widget('bootstrap.widgets.TbMenu', array(
                            'items' => $mainMenuItems,
                        ));
                        ?>
                        
                        <ul id="yw1" class="pull-right nav">
                            <?php if (Yii::app()->user->isGuest) { ?>
                                <li><a href="<?= $this->createUrl('/login/index')?>">Войти</a></li>
                            <? } else { ?>
                                <li class="dropdown">
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                        <img title="<?= Yii::app()->user->name ?>" alt="<?= Yii::app()->user->name ?>" src="<?=Yii::app()->user->getModel()->profile_image_url?>">
                                        <span class="avatar" title="Уровень <?=Yii::app()->user->getModel()->level?>"><?= Yii::app()->user->name ?><?if (Yii::app()->user->getModel()->level) echo "<sup>".Yii::app()->user->getModel()->level."</sup>"; ?></span> <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?= $this->createUrl('/login/login')?>" tabindex="-1">Подключить другие сервисы</a></li>
                                        <li><a href="<?= $this->createUrl('/users/apps')?>" tabindex="-1">Приложения</a></li>
                                        <li><a href="<?= $this->createUrl('/login/logout')?>" tabindex="-1">Выйти</a></li>
                                    </ul>
                                </li>
                            <?php } ?>
                        </ul>
                        
                        <?php 
                        if (!Yii::app()->user->isGuest) {
                            $curPoints = Yii::app()->user->getModel()->countPoints();
                            $level = Yii::app()->user->getModel()->level;
                            $leftBoundPoints = User::getAmountPointsForLevel($level);
                            $rightBoundPoints = UseR::getAmountPointsForLevel($level + 1);
                            $rightBoundPointsLevel = $rightBoundPoints - $leftBoundPoints;
                            $curPointsLevel = $curPoints - $leftBoundPoints;  
                            $percent = round($curPointsLevel / $rightBoundPointsLevel * 100);
                            ?> 
                                <div class="progress user-level-progress span2 pull-right" title="Прогресс вашего уровня: <?= $curPoints ?>/<?= $rightBoundPoints ?>">
                                    <div class="bar" style="width: <?=$percent?>%;"><?= $curPointsLevel ?>/<?= $rightBoundPointsLevel ?></div>
                                </div>
                        <?php } ?>
                        
                    </div><!--/.nav-collapse -->  
                </div>  
            </div>  
        </div>
        
        
        
        <div class="container">  
            
            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                ));
                ?><!-- breadcrumbs -->
            <?php endif ?>
                
            <div class="row">
                <div class="span12">
                    <?php echo $content; ?>
                </div>
            </div>
            
            <hr>  
            <footer>  
                <p>© <a href="https://twitter.com/LiveLevelNet" target="_blank">Livelevel.net</a> 2013.</p>
                <p><?=CHtml::link('Правила', array('/site/page/view/rules'))?></p>
                <p><?=CHtml::link('Разработчикам', array('/dev'))?></p>  
            </footer>  
        </div> <!-- /container -->  
        <?php if (!(defined('YII_DEBUG') && YII_DEBUG)) { ?>
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter20560975 = new Ya.Metrika({id:20560975,
                            clickmap:true,
                            trackHash:true});
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript><div><img src="//mc.yandex.ru/watch/20560975" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
        <?php } ?>
    </body>  
</html>