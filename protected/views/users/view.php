<?php
/* @var $user User */
$this->pageTitle = Yii::app()->name . ' - Профиль пользователя '. CHtml::encode($user->screen_name);
?>
<div class="row">
    <div class="span6 well">
        <div class="row">
            <div class="span1">
                <a href="<?= CHtml::encode($user->getExternalProfileUrl()) ?>" class="thumbnail">
                    <img src="<?= CHtml::encode($user->profile_image_url) ?>" alt="<?= CHtml::encode($user->screen_name) ?>"></a>
            </div>
            <div class="span5">
                <p><?= CHtml::encode($user->screen_name) ?>
                <?php if (Yii::app()->user->checkAccess('banUser') && !$user->banned) { ?>
                    <a href="<?=Yii::app()->createUrl('/users/ban', array('userid'=>$user->id))?>" class="btn btn-danger btn-mini">Заблокировать</a>
                <?php } ?>
                </p>
                <p><strong><?= CHtml::encode($user->full_name) ?></strong>, уровень <?=$user->level?></p>
                <span class="badge badge-warning">Достижений добавил: <?= $addedCount ?></span> 
                <span class=" badge badge-info">Достижений выполнено и подтверждено: <?= $achievedCount ?></span>
                <span class=" badge badge-success">Очков: <?= (int)$user->countPoints() ?></span>
            </div>
        </div>
    </div>
</div>

<ul class="nav nav-tabs">
    <li class="<?php if ($activeTab == 'achievedList') echo 'active'; ?>"><a href="#achievedList" data-toggle="tab">Выполеннные и подтверждённые достижения</a></li>
    <li class="<?php if ($activeTab == 'addedList') echo 'active'; ?>"><a href="#addedList" data-toggle="tab">Добавленные достижения</a></li>
</ul>

<div class="tab-content">
    <div class="tab-pane <?php if ($activeTab == 'achievedList') echo 'active'; ?>" id="achievedList">
        <?php if (count($achievedList)) { ?>
            <?php foreach ($achievedList as $ach) { ?>
                <?php $this->widget('widgets.achievement.AchWidget', array('model' => $ach, 'user' => $user)); ?>
            <?php } ?>

            <?php
            $this->widget('CLinkPager', array(
                'pages' => $achievedPages,
            ))
            ?>
        <? } else { ?>
            <em>Таких ещё нет.</em>
        <?php } ?>
    </div>

    <div class="tab-pane <?php if ($activeTab == 'addedList') echo 'active'; ?>" id="addedList">
        <?php if (count($addedList)) { ?>
            <?php foreach ($addedList as $ach) { ?>
                <?php $this->widget('widgets.achievement.AchWidget', array('model' => $ach, 'user' => $viewUser)); ?>
            <?php } ?>

            <?php
            $this->widget('CLinkPager', array(
                'pages' => $addedPages,
            ))
            ?>

        <?php } else { ?>
            <em>Таких ещё нет.</em>
        <?php } ?>
    </div>
</div>