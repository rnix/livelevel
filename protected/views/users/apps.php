<?php
/* @var $this UsersController */
/* @var $models UserApp */

$this->pageTitle = Yii::app()->name . ' - Приложения';
?>
<h3>Список сторонних приложений, которым вы предоставили доступ</h3>

<?php
$this->widget('bootstrap.widgets.TbAlert', array(
    'alerts' => array('success' => array('block' => true, 'fade' => true, 'closeText' => '&times;')),
));
?>

<?php if (count($models)) { ?>

<table class="table">
    
    <tr>
        <th>Название</th>
        <th>Дата предоставления доступа</th>
        <th></th>
    </tr>
        
    <?php foreach ($models as $model) { ?>
    <tr>
        <td><?=CHtml::encode($model->app->app_title)?></td>
        <td><?=$model->grant_date?></td>
        <td><?=CHtml::link('убрать доступ', array('users/revokeApp', 'appId'=>$model->client_id), array('class'=>'btn btn-danger'))?></td>
    </tr>
    <?php } ?>
    
</table>

<?php } else {  ?>

<em>Вы не предоставили доступ еще ни одному стороннеу приложению.</em>

<?php } ?>


<?php if (count($myApps)) { ?>
<h3>Мои приложения</h3>
<table class="table">
    
    <tr>
        <th>Название</th>
        <th>Дата создания</th>
        <th></th>
    </tr>
        
    <?php foreach ($myApps as $model) { ?>
    <tr>
        <td><?=CHtml::encode($model->app_title)?></td>
        <td><?=$model->created_at?></td>
        <td><?=CHtml::link('редактировать', array('dev/EditApp/'.$model->client_id), array('class'=>'btn'))?></td>
    </tr>
    <?php } ?>
    
</table>

<?php } ?>
