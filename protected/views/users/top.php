<?php 
/* @var $users User[]*/
$this->pageTitle = Yii::app()->name . ' - Топ пользователей';
?>
<h2>Топ пользователей</h2>
<?php if (count($users)) { ?>
    <table class="table">
        <tr>
            <th>#</th>
            <th>Пользователь</th>
            <th>Уровень</th>
            <th>Очки</th>
            <th>Добавил достижений</th>
            <th>Выполнил достижений</th>
            <th>Дата регистрации</th>
        </tr>

        <?php $i = 0;
        foreach ($users as $user) {
            $i++; ?>
            <tr>
                <td><?= $i ?></td>
                <td><?php $this->widget('widgets.user.UserWidget', array( 'model' => $user, 'showLevel' => false));?></td>
                <td><?php echo (int)$user->level;?></td>
                <td><?php echo (int)$user->countPoints();?></td>
                <td><?php echo count($user->achievementsAuthor);?></td>
                <td><?php echo $user->countAchievedAndProoved();?></td>
                <td><?php echo Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($user->ins_date), 'medium', null); ?></td>
            </tr>
    <?php } ?>
    </table>
<?php } else { ?>
<em>Список ещё пуст. Он ждёт своих чемпионов. Может вы станете им? Получите первый уровень!</em>
<?php } ?>

