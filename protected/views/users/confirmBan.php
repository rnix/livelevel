<?php 
/* @var $model User */
?>
<h2>Действительно заблокировать?</h2>
<?php $this->beginWidget('bootstrap.widgets.TbActiveForm'); ?>
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Да, заблокировать', 'htmlOptions' => array('class' => 'btn-danger', 'name'=>'confirm', 'value'=>'yes'))); ?>
<?php $this->endWidget(); ?>

<?php $this->widget('widgets.user.UserWidget', array('model' => $model));?>
