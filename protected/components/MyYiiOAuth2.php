<?php

class MyYiiOAuth2 extends YiiOAuth2 {

    protected $tokenInfo;
    static private $instance = false;
    
    public static function instance() {
      if(MyYiiOAuth2::$instance === false){
          MyYiiOAuth2::$instance = new MyYiiOAuth2(array('display_error'=>true));
      }
      return MyYiiOAuth2::$instance;
  }

    /**
     * Implements OAuth2::checkClientCredentials().
     *
     * Do NOT use this in production! This sample code stores the secret
     * in plaintext!
     */
    protected function checkClientCredentials($client_id, $client_secret = NULL) {
        try {
            $sql = "SELECT client_secret FROM oauth2_clients WHERE client_id = :client_id";
            $connection = Yii::app()->db;
            $stmt = $connection->createCommand($sql);
            $stmt->bindParam(":client_id", $client_id, PDO::PARAM_STR);

            $result = $stmt->queryRow();

            if ($client_secret === NULL)
                return $result !== FALSE;

            return $result["client_secret"] == $client_secret;
        } catch (Exception $e) {
            $this->handleException($e);
        }
    }
    
    protected function getAccessToken($oauth_token) {
        return $this->getToken($oauth_token, YiiOAuth2::TOKEN_TYPE_ACCESS_TOKEN);
    }

    private function getToken($oauth_token, $token_type = YiiOAuth2::TOKEN_TYPE_ACCESS_TOKEN) {
        try {
            $sql = "SELECT * FROM oauth2_tokens WHERE oauth_token = :oauth_token AND token_type=:token_type";

            $connection = Yii::app()->db;
            $stmt = $connection->createCommand($sql);

            $stmt->bindParam(":oauth_token", $oauth_token, PDO::PARAM_STR);
            $stmt->bindParam(":token_type", $token_type, PDO::PARAM_STR);

            $result = $stmt->queryRow();
            
            if ($result){
                $this->tokenInfo['client_id'] = $result['client_id'];
                $this->tokenInfo['user_id'] = $result['user_id'];
                
                //delete all others tokens
                if ($token_type == YiiOAuth2::TOKEN_TYPE_ACCESS_TOKEN) {
                    $command = Yii::app()->db->createCommand();
                    $command->delete('oauth2_tokens', 'client_id=:client_id AND user_id=:user_id AND oauth_token!=:oauth_token', array(
                        ':client_id' => $result['client_id'],
                        ':user_id' => $result['user_id'],
                        ':oauth_token' => $result['oauth_token'],
                    ));
                }
            }

            return $result !== FALSE ? $result : NULL;
        } catch (PDOException $e) {
            $this->handleException($e);
        }
    }
    
    public function getTokenInfo(){
        return $this->tokenInfo;
    } 
    
    protected function getSupportedGrantTypes() {
        return array(
            OAUTH2_GRANT_TYPE_AUTH_CODE,
            OAUTH2_GRANT_TYPE_CLIENT_CREDENTIALS,
        );
    }
    
    public function verify() {
        if ($this->verifyAccessToken())
            return $this->getVariable('user_id');
        return FALSE;
    }
}
