<?php

class ApiError {
    
    public $error_name;
    public $error_code;
    public $error_msg;
    public $info;        
    
    public function __construct($name, $desc='', $info = null) {
        $this->error_name = $name;
        $this->error_msg = $desc;
        $this->info = $info;
    }
    
    public function __toString() {
        $er = new stdClass();
        $er->error = $this;
        return CJSON::encode($er);
    }
    
}