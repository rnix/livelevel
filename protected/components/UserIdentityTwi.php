<?php

class UserIdentityTwi extends UserIdentity {
    
    public function __construct($authIdentity = null) {
        parent::__construct($authIdentity);
        $this->_provider = 'twi';
    }
    
    public function authenticate() {
        $access_token = Yii::app()->session['access_token'];
        $connection = new twitteroauth(
                Yii::app()->params['private']['CONSUMER_KEY'], 
                Yii::app()->params['private']['CONSUMER_SECRET'], 
                $access_token['oauth_token'], 
                $access_token['oauth_token_secret']);
        
        $response = $connection->get('account/verify_credentials');
        if (is_object($response) && !empty($response->id)){
            $this->_extId = $response->id;
            
            if (!Yii::app()->user->isGuest && Yii::app()->user->getModel()->external_id_vk) {
                $user = User::model()->findByAttributes(array('external_id_vk' => Yii::app()->user->getModel()->external_id_vk));
                $user->external_id_twi = $this->_extId;
            } else {
                $user = User::model()->findByAttributes(array('external_id_twi' => $this->_extId));
                if ($user === null) {
                    $user = new User;
                    $user->external_id_twi = $this->_extId;
                }
            }
            
            $user->screen_name = $response->screen_name;
            $user->profile_image_url = $response->profile_image_url;
            $user->full_name = $response->name;
            $user->lang = $response->lang;
            $user->location = $response->location;
            
            $user->save();
            $this->_id = $user->id;
            $this->_name = $user->screen_name;
            $this->_saveState();
            $this->errorCode = self::ERROR_NONE;
        } else {
            if (is_object($response) && isset($response->errors)){
                $error = $response->errors[0];
                Yii::log($error->message, CLogger::LEVEL_ERROR, 'AUTH');
            } else {
                Yii::log('Invalid response from Twitter', CLogger::LEVEL_ERROR, 'AUTH');
            }
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }
        return !$this->errorCode;
    }
    
}