<?php

class PhpAuthManager extends CPhpAuthManager{
    public function init(){
        parent::init();
 
        if(!Yii::app()->user->isGuest){
            $role = Yii::app()->user->getRole();
            if (!$role){
                $role = 'user';
            }
            $this->assign($role, Yii::app()->user->id);
        }
    }
}