<?php

/**
 * I need public $service for getting data
 * through EAuthUserIdentity.
 */

class MyEAuthUserIdentity extends EAuthUserIdentity {
    
	/**
	 * @var EAuthServiceBase the authorization service instance.
	 */
	public $service;
}
