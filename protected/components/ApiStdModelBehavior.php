<?php

/**
 * Provides method for safe converting model into StdClass
 * for API methods.
 * 
 */
class ApiStdModelBehavior extends CActiveRecordBehavior {
    
    public $publicApiProperties = array();
    
    /**
     * 
     * @param mix $additionalProperties
     * Example:
     * array('need_proof', 'app'=>array('app_title')) for Achievement
     * returns value of property "need_proof", adds related model "app"
     * with value of "app_title".
     * 
     * @return \stdClass
     */
    public function convertToApiStdClass($additionalProperties = array()) {
        $baseModel = $this->Owner;
        $publicProperties = $this->publicApiProperties;
        
        $std = new stdClass();
        foreach ($publicProperties as $alias=>$property) {   
            
            $fieldName = $property;
            if (!is_numeric($alias)){
                $fieldName = $alias;
            }
            
            if (method_exists($baseModel, $property)){
                $std->$fieldName = $baseModel->$property();
            } else if (is_array($baseModel->$property)) {             
                $array = array();
                    
                $conditionMethod = "getApiRelated". ucfirst($property);
                if (method_exists($baseModel, $conditionMethod)) {
                    /* use condition or order for related models  if method exists */
                    foreach ($baseModel->$conditionMethod() as $subClass) {
                        $array[] = $subClass->convertToApiStdClass();
                    }
                } else {
                    /* use all rows of related model */
                    foreach ($baseModel->$property as $subClass) {
                        $array[] = $subClass->convertToApiStdClass();
                    }
                }
                
                $std->$fieldName = $array;
            } else {
                $std->$fieldName = $baseModel->$property;
            }
        }
        
        foreach ($additionalProperties as $key=>$value) {
            if (is_array($value)){
                $property = $key;
                if (is_array($baseModel->$property)) {
                    $array = array();
                    foreach ($baseModel->$property as $subClass) {
                        $array[] = $subClass->convertToApiStdClass($value);
                    }
                    $std->$property = $array;
                } else if (is_object($baseModel->$property)) {
                    $std->$property = $baseModel->$property->convertToApiStdClass($value);
                } else {
                    $std->$property = $baseModel->$property;
                    
                }
            } else {
                $property = $value;
                $std->$property = $baseModel->$property;
            }
        }
        
        return $std;
    }
    
}
