<?php

class MyVKontakteService extends VKontakteOAuthService {

	protected function fetchAttributes() {
		$info = (array)$this->makeSignedRequest('https://api.vk.com/method/users.get.json', array(
			'query' => array(
				'uids' => $this->uid,
				'fields' => 'screen_name, city, country, photo_50',
			),
		));

		$info = $info['response'][0];

		$this->attributes['id'] = $info->uid;
		$this->attributes['full_name'] = $info->first_name.' '.$info->last_name;
		$this->attributes['screen_name'] = $info->screen_name;
		$this->attributes['profile_image_url'] = $info->photo_50;
                
                $location = array();
                
                $cityInfo = (array)$this->makeSignedRequest('https://api.vk.com/method/getCities', array(
			'query' => array(
				'cids' => $info->city,
			),
		));
                $cityInfo = $cityInfo['response'][0];
                if ($cityInfo && $cityInfo->name){
                    $location[] = $cityInfo->name;
                }
                
                $countyInfo = (array)$this->makeSignedRequest('https://api.vk.com/method/getCountries', array(
			'query' => array(
				'cids' => $info->country,
			),
		));
                $countyInfo = $countyInfo['response'][0];
                if ($countyInfo && $countyInfo->name){
                    $location[] = $countyInfo->name;
                }
                
                
		$this->attributes['location'] = implode(', ', $location);
	}
}
