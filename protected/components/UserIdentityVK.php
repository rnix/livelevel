<?php

class UserIdentityVK extends UserIdentity {

    public function __construct($authIdentity = null) {
        parent::__construct($authIdentity);
        $this->_provider = 'vk';
    }
    
    public function authenticate() {
        $authIdentity = new MyEAuthUserIdentity($this->_authIdentity);
        if ($authIdentity->authenticate()) {
            $authIdentity = $authIdentity->service;
            $this->_extId = $authIdentity->id;
            
            if (!Yii::app()->user->isGuest && Yii::app()->user->getModel()->external_id_twi) {
                $user = User::model()->findByAttributes(array('external_id_twi' => Yii::app()->user->getModel()->external_id_twi));
                $user->external_id_vk = $this->_extId;
            } else {
                $user = User::model()->findByAttributes(array('external_id_vk' => $this->_extId));
                if ($user === null) {
                    $user = new User;
                    $user->external_id_vk = $this->_extId;
                }
                $user->screen_name = $authIdentity->screen_name;
            }
            
            $user->profile_image_url = $authIdentity->profile_image_url;
            $user->full_name = $authIdentity->full_name;
            $user->location = $authIdentity->location;
            $user->save();
            
            $this->_id = $user->id;
            $this->_name = $user->screen_name;
            $this->_saveState();
            $this->errorCode = self::ERROR_NONE;
            return !$this->errorCode;
        } else {
            return false;
        }
    }

}