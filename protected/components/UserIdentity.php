<?php

class UserIdentity extends CBaseUserIdentity {

    protected $_id;
    protected $_extId;
    protected $_name;
    protected $_authIdentity;
    protected $_provider;
    
    public function __construct($authIdentity = null) {
        $this->_authIdentity = $authIdentity;
    }
    
    public function getId() {
        return $this->_id;
    }
    
    public function getName(){
        return $this->_name;
    }
    
    public function authenticate(){
        
    }
    
    protected function _saveState() {
        $this->setState('id', $this->_id);
        $this->setState('name', $this->_name);
        $this->setState('provider', $this->_provider);
        $this->setState('extId', $this->_extId);
    }
    
}