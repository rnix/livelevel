<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    private $_assetsCssBase;
    private $_assetsJsBase;

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    public function getAssetsCssBase() {
        if ($this->_assetsCssBase === null) {
            $this->_assetsCssBase = Yii::app()->assetManager->publish(
                    Yii::getPathOfAlias('application.assets.css'), false, -1, defined('YII_DEBUG') && YII_DEBUG
            );
        }
        return $this->_assetsCssBase;
    }
    
    public function getAssetsJsBase() {
        if ($this->_assetsJsBase === null) {
            $this->_assetsJsBase = Yii::app()->assetManager->publish(
                    Yii::getPathOfAlias('application.assets.js'), false, -1, defined('YII_DEBUG') && YII_DEBUG
            );
        }
        return $this->_assetsJsBase;
    }
    
    public function beforeAction($action){
        parent::beforeAction($action);
        
        if (!Yii::app()->user->isGuest){
            if (Yii::app()->user->getModel()->banned && !($this->id == 'users' && $action->id == 'banned')){
                $this->redirect('/users/banned');
            }
        }
        return true;
    }

}