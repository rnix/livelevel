<?php

class StatusWidget extends CWidget {
    
    public $status;
    public $userAchievement;
    public $value = 0;

    public function init() {
        // this method is called by CController::beginWidget()
    }

    public function run() {
        // this method is called by CController::endWidget()
        $this->render('StatusView', array(
            'status' => $this->status,
            'value' => $this->value,
            'userAchievement' => $this->userAchievement,
        ));
    }

}