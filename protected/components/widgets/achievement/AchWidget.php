<?php

class AchWidget extends CWidget {

    public $model;
    public $user;
    public $dependsCollapsed = true;

    public function init() {
        // this method is called by CController::beginWidget()
    }

    public function run() {
        // this method is called by CController::endWidget()
        $this->render('AchView', array(
            'model' => $this->model,
            'user' => $this->user,
            'dependsCollapsed' => $this->dependsCollapsed,
        ));
    }

}