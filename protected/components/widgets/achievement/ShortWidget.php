<?php

class ShortWidget extends CWidget {

    public $model;
    
    public function run() {
        $this->render('ShortView', array(
            'model' => $this->model,
        ));
    }

}