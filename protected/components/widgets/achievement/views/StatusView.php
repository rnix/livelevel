<?php
/* @var $userAchievement UserAchievement */
?>

<?php if ($userAchievement && $userAchievement->status == UserAchievement::STATUS_APPROVED) { ?>
    <span class="label label-success" title="Проверено">
        Достигнуто<br>
        <?php echo $userAchievement->ach_date; ?>
    </span>
<?php } else if ($userAchievement && !$userAchievement->status) { ?>
    <span class="label" title="Не проверено">
        Не проверено<br> 
        <?php echo $userAchievement->ach_date; ?>
    </span>
<?php } ?>


