<?php
/* @var $model Achievement */
/* @var $user User */
?>

<form class="ach-container">
    <fieldset>
        <legend>id<?= $model->id ?></legend>
        <div class="row-fluid">
            <div class="span9">
                <h4><a href="/id<?= $model->id ?>"><?= CHtml::encode($model->name) ?></a></h4>
            </div>
            <div class="ach-status-container span3">
                <?php $this->widget('widgets.achievement.StatusWidget', array(
                'userAchievement' => $model->getUserAchievement($user),
                    )); ?>
            </div>

        </div>
        <p>
            <?= CHtml::encode($model->desc) ?>
        </p>
        <div class="tags">
            <?php foreach ($model->tags as $tag) { ?>
                <a href="<?= Yii::app()->createUrl('achievement/index', array('#' => 'tags=' . CHtml::encode($tag->name))); ?>"
                   data-tag="<?= CHtml::encode($tag->name) ?>"
                   >#<?= CHtml::encode($tag->name) ?></a>
            <?php } ?>
        </div>

        <?php if ($model->app_client_id) { ?>
            <div><a href="<?=Yii::app()->createUrl('/app/'.$model->app_client_id)?>" class="label label-inverse" title="Управляется приложением"><i class="icon-white icon-share-alt"></i><?=CHtml::encode($model->app->app_title)?></a></div>
        <?php } ?>
            
        <?php if (!$model->public) { ?>
            <div><span class="label">Скрыто</span></div>
        <?php } ?>

        <?php if (count($model->achievementDepends)) { ?>
        <div id="accordion2" class="accordion">

            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle <?php if ($dependsCollapsed) echo 'collapsed';?>" data-toggle="collapse" data-parent="#accordion2" href="#collapseAch<?=$model->id?>">
                        <i class="icon-plus-sign"></i><i class="icon-minus-sign"></i> Требования для этого достижения
                    </a>
                </div>
                <div class="accordion-body collapse <?php if (!$dependsCollapsed) echo 'in';?>" id="collapseAch<?=$model->id?>">
                    <div class="accordion-inner">
                      <div class="us">
                        <table class="table table-striped">
                                <?php $i=0; foreach ($model->achievementDepends as $achD) { $i++; ?>
                                    <tr>
                                        <td class="muted"><?=$i?></td>
                                        <td>id<?= $achD->achDepends->id ?></td>
                                        <td>
                                            <a href="/id<?= $achD->achDepends->id ?>"><?= Chtml::encode($achD->achDepends->name) ?></a>
                                        </td>
                                        <td>
                                            <?php if ($achD->achDepends->isAchievedAndApproved($user)) { ?>
                                                <i class="icon-ok"></i>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                         </table>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
        <?php } ?>
        <?php if (Yii::app()->user->checkAccess('deleteAchievement', array('achievement'=>$model))) { ?>
        <div class="pull-right">
            <a class="btn btn-danger btn-mini" href="<?=Yii::app()->createUrl('/achievement/delete', array('achid'=>$model->id))?>">удалить</a>
        </div>
        <?php } ?>
    </fieldset>
</form>