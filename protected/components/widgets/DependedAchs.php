<?php

Yii::import('ext.select2.ESelect2');

class DependedAchs extends ESelect2 {

    public $preselectedIds = '';
    
    public function init() {
        parent::init();
        $this->options = array(
            'allowClear' => true,
            'minimumInputLength' => 1,
            'width' => '600',
            'ajax' => array(
                'url' => Yii::app()->createUrl('json/SuggestAchs'),
                'dataType' => 'json',
                'data' => 'js:function(term,page) { return {q: term, page_limit: 3, page: page}; }',
                'results' => 'js:function(data,page) { return {results: data}; }',
                'messages' => '{results: ""}',
            ),
            'multiple'=>true,
            'initSelection' => 'js:function (element, callback) {
        var selected_data = [];
        $.ajax({
            type: "GET",
            url: "' . Yii::app()->createUrl('json/getAchsByIds') . '",
            data: "ids=' . $this->preselectedIds . '",
            dataType: "json",
            success: function(result) { 
                for (var key in result) {
                    if (result.hasOwnProperty(key)) {
                        selected_data.push({id: result[key].id, text: result[key].text});
                        callback(selected_data);
                    }
                }
            }
        });
    }',
        );
    }
}