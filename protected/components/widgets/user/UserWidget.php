<?php

class UserWidget extends CWidget {
    
    public $model;
    public $showLevel = true;

    public function run() {
        $this->render('UserView', array(
            'model' => $this->model,
            'showLevel' => $this->showLevel,
        ));
    }

}